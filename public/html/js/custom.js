$(document).ready(function(){
	"use strict";
	$(window).scroll(function(){
		var checkHeight = $(window).scrollTop();
		if(checkHeight > 70)
			{
				$('.navbar-fixed-top').addClass("sticky");
			}
		else
			{
				$('.navbar-fixed-top').removeClass("sticky");
			}
	});
	jQuery('body').scrollspy({target: ".navbar", offset: 50});   
	jQuery(".nav a").on('click', function(event) {
		if (this.hash !== "") {
		  event.preventDefault();
		  var hash = this.hash;
		  jQuery('html, body').animate({
			scrollTop: jQuery(hash).offset().top
		  }, 500, function(){
	//        window.location.hash = hash;
		  });
		}
	  });
});