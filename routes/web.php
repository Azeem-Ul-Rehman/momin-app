<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    if (auth()->check()) {

        return redirect()->route('admin.dashboard.index');
    } else {
        return view('auth.login');
    }

})->name('index');


Auth::routes(['verify' => true]);
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
//    \Illuminate\Support\Facades\Artisan::call('storage:link');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');

//    \Illuminate\Support\Facades\Artisan::call('passport:install');
//    \Illuminate\Support\Facades\Artisan::call('migrate');

    return 'Commands run successfully Cleared.';
});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register/{referral_code?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


//Forget Password
Route::get('/user/verify/{token}', 'Auth\LoginController@activation');


Route::post('otp', 'Auth\ForgotPasswordController@otp')->name('otp.send');
Route::post('verify/otp', 'Auth\ForgotPasswordController@verifyOTP')->name('verify.otp');
Route::post('forgot-password-request', 'Auth\ForgotPasswordController@ForgotPasswordRequest')->name('otp.password.reset');

Route::get('/home', 'HomeController@index')->name('home');

//Landing Page Routes

Route::get('about-us', 'Frontend\AboutUsController@index')->name('aboutus.detail');
Route::get('how-it-works', 'Frontend\AboutUsController@howItWorks')->name('how.its.works.detail');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/meta-tags', 'Backend\MetaTagController', [
            'only' => ['index', 'create', 'store', 'edit', 'update']
        ]);
        Route::resource('/cities', 'Backend\CityController');
        Route::resource('/states', 'Backend\StateController');

        //Blogs Routes Start
        Route::resource('/blogs', 'Backend\BlogController');
        Route::post('add/blog/comments', 'Backend\BlogController@addComment')->name('add.blogs.comments');
        Route::get('view/comments/{blog_id}', 'Backend\BlogController@viewComments')->name('view.blogs.comments');
        Route::get('update/blog/comment/status', 'Backend\BlogController@updateStatus')->name('update.blog.comment.status');
        Route::delete('blog/comments/delete/{blog_id}', 'Backend\BlogController@deleteComment')->name('blogs.comment.destroy');
        //Blogs Routes End

        //Events Routes Start
        Route::resource('/events', 'Backend\EventController');
        Route::get('event/gallery/add/{event_id}', 'Backend\EventController@addEventGallery')->name('event.gallery.add');
        Route::get('event/gallery/view/{event_id}', 'Backend\EventController@viewEventGallery')->name('event.gallery.view');
        Route::post('event/gallery/store', 'Backend\EventController@storeEventGallery')->name('event.gallery.store');
        Route::post('event/gallery/delete', 'Backend\EventController@destroyEventGallery')->name('event.gallery.destroy');
        Route::delete('event/gallery/single/delete/{event_id}', 'Backend\EventController@singleDestroyEventGallery')->name('event.gallery.single.destroy');
        Route::resource('/event-galleries', 'Backend\EventGalleryController');
        //Events Routes End


        //Library Dua Routes Start
        Route::resource('/dua-categories', 'Backend\DuaCategoryController');
        Route::resource('/dua-sub-categories', 'Backend\DuaSubCategoryController');
        //Library Dua Routes End


        //Momin TV Routes Start
        Route::resource('/momin-tv', 'Backend\MominTvCategoryController');
        Route::resource('/videos', 'Backend\MominTvVideoController');
        //Momin TV Routes End

        Route::post('/states/import/file', 'Backend\StateController@importFile')->name('states.import.excel');
        Route::resource('/users', 'Backend\UserController');
        Route::post('/users/location', 'Backend\UserController@updateLocation')->name('user.location');


        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'edit', 'update']
        ]);
        Route::resource('/contacts', 'Backend\ContactUsController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('/aboutus', 'Backend\AboutUsController', [
            'only' => ['index', 'store']
        ]);
        Route::resource('/banner', 'Backend\BannerController', [
            'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
        ]);

        Route::resource('/privacypolicy', 'Backend\PrivacyController', [
            'only' => ['index', 'store']
        ]);

        //User Profiles
        Route::get('profile', 'Backend\ProfileController@index')->name('profile.index');

        Route::get('edit/profile', 'Backend\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Backend\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Backend\ProfileController@updateProfile')->name('update.user.profile');

        Route::get('update/password/', 'Backend\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Backend\ProfileController@updatePassword')->name('update.user.password');

        Route::get('update/phone', 'Backend\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Backend\ProfileController@updateMobileNumber')->name('update.user.phone');


        //Aal-e-Muhammad
        Route::resource('aal-e-muhammad', 'Backend\AaleMuhammadController');

        //Calender Events
        Route::resource('calendar-events', 'Backend\CalenderEventController');

        //Hadees
        Route::resource('hadees', 'Backend\HadeesController');


    });

    Route::get('/sendsms/{phone}/{message}', 'Backend\SendReferralCodeController@sendCodeWithPhone');
});

Route::get('privacy-policy', function () {
    $privacyPolicy = \App\Models\PrivacyPolicy::first();
    return view('frontend.pages.privacy-policy', compact('privacyPolicy'));
})->name('privacy.policy');


//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {

    Route::get('state-cities', 'Backend\AjaxController@stateCities')->name('stateCities');
});
Route::resource('/contacts', 'Backend\ContactUsController', [
    'only' => ['create', 'store']
]);
Route::get('/contacts', 'Backend\ContactUsController@create')->name('contacts.create');
Route::post('/contacts/store', 'Backend\ContactUsController@store')->name('contacts.store');




