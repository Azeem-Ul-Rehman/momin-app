<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('otp', 'Api\AuthController@otp');
Route::post('verify/otp', 'Api\AuthController@verifyOTP');
Route::post('forgot-password-request', 'Api\AuthController@forgotPasswordRequest');
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

Route::get('states', 'Api\GeneralController@states');
Route::get('cities/{state_id}', 'Api\GeneralController@cities');
Route::get('settings', 'Api\GeneralController@settings');
Route::get('quran-chapters', 'Api\GeneralController@getQuranChapters');
Route::get('get-verses-by-chapter/{chapter_id}', 'Api\GeneralController@getVersesByChapter');


Route::get('prayer/timings', 'Api\GeneralController@prayerTimings');
Route::get('quran/{language}', 'Api\GeneralController@getQuran');


//Social Login/SignUp Start
Route::post('social-login-register', "Api\AuthController@socialLoginRegister");
//Social Loign/SignUp End



//Content Route Start

Route::get('banner', 'Api\GeneralController@banner');
Route::get('about-us', 'Api\GeneralController@aboutUs');
//Content Route End


//Aal-e-Muhammad
Route::get('aal-e-muhammad', 'Api\GeneralController@getAaleMuhammad');

//Calender Events
Route::get('calender-events', 'Api\GeneralController@calenderEvents');


//Hadees Books
Route::get('hadees/books', 'Api\GeneralController@getHadeesBooks');



//Events
Route::get('events', 'Api\GeneralController@getEvents');


Route::get('family-tree', 'Api\GeneralController@familyTree');

//Dua
Route::get('dua/category', 'Api\GeneralController@duaCategory');
Route::get('dua/sub/category/{category_id}', 'Api\GeneralController@duaSubcategory');

//Momin TV
Route::get('momin/tv/category', 'Api\GeneralController@mominTvCategory');
Route::get('momin/tv/videos/{category_id}', 'Api\GeneralController@mominTvVideos');



Route::group(['middleware' => 'auth:api'], function () {
    Route::post('update-profile', 'Api\GeneralController@updateProfile');
    Route::post('update-password', 'Api\GeneralController@updatePassword');
    Route::post('update-profile-image', 'Api\GeneralController@updateProfileImage');

    Route::post('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@getUser');

    Route::post('get-lat-lng', 'Api\GeneralController@getLatLong');
    Route::post('store-lat-lng', 'Api\GeneralController@saveLatLong');

    Route::prefix('realtor')->group(function () {
        Route::get('notifications ', 'Api\RealtorController@notifications');

    });

    //Blogs

    Route::get('blogs', 'Api\GeneralController@getBlogs');
    Route::post('blog/add/comment', 'Api\GeneralController@addComment');
    Route::post('blog/delete/comment', 'Api\GeneralController@deleteComment');
    Route::post('blog/like', 'Api\GeneralController@blogLike');
    Route::post('blog/unlike', 'Api\GeneralController@blogUnLike');

    //Events
    Route::post('check/user/is-going', 'Api\GeneralController@checkUserIsGoing');
    Route::post('event/going', 'Api\GeneralController@going');
    Route::post('event/not/going', 'Api\GeneralController@notGoing');

});

Route::fallback(function () {
    return response()->json(['message' => 'URL Not Found'], 404);
});
