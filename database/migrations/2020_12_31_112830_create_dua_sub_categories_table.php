<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDuaSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dua_sub_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->text('title');
            $table->longText('description_en')->nullable();
            $table->text('audio_en')->nullable();
            $table->longText('description_ar')->nullable();
            $table->text('audio_ar')->nullable();
            $table->longText('description_ur')->nullable();
            $table->text('audio_ur')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dua_sub_categories');
    }
}
