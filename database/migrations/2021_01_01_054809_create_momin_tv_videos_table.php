<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMominTvVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('momin_tv_videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('momin_tv_category_id');
            $table->text('title');
            $table->text('short_description');
            $table->text('link');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('momin_tv_videos');
    }
}
