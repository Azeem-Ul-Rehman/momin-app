<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChapterVersesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter_verses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('chapter_id');
            $table->unsignedInteger('verse_number');
            $table->text('verse_key')->nullable();
            $table->text('text_madani')->nullable();
            $table->text('text_indopak')->nullable();
            $table->text('text_simple')->nullable();
            $table->text('text_english')->nullable();
            $table->text('juz_number')->nullable();
            $table->text('hizb_number')->nullable();
            $table->text('rub_number')->nullable();
            $table->text('sajdah')->nullable();
            $table->text('sajdah_number')->nullable();
            $table->text('page_number')->nullable();
            $table->text('audio_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter_verses');
    }
}
