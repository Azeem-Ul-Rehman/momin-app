<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'name' => 'Helpline Support',
            'value' => '0000 0000000',
        ]);

        Setting::create([
            'name' => 'Email Support',
            'value' => 'info@mominapp.com',
        ]);

        Setting::create([
            'name' => 'Find Us At',
            'value' => 'xyz address',
        ]);

        Setting::create([
            'name' => 'Suggestion',
            'value' => 'info@mominapp.com',
        ]);
        Setting::create([
            'name' => 'Bug Report',
            'value' => 'info@mominapp.com',
        ]);

        Setting::create([
            'name' => 'Complaint',
            'value' => 'info@mominapp.com',
        ]);

        Setting::create([
            'name' => 'Feature Request',
            'value' => 'info@mominapp.com',
        ]);

        Setting::create([
            'name' => 'Other',
            'value' => 'info@mominapp.com',
        ]);

        Setting::create([
            'name' => 'Whatsapp',
            'value' => 'https://web.whatsapp.com',
        ]);

        Setting::create([
            'name' => 'Instagram',
            'value' => 'http://instagram.com',
        ]);

        Setting::create([
            'name' => 'Facebook',
            'value' => 'http://facebook.com',
        ]);


    }
}
