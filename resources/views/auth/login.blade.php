@extends('frontend.layout.master')
@section('title','Login')
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Sign In</h3>
            </div>
        </div>
    </div>

    <div class="loginArea">
        <div class="container">
            <div class="col-md-6 col-sm-5 col-xs-12 loginImg">

            </div>

            <div class="col-md-6 col-sm-7 col-xs-12">
                <h2 class="heading">sign in to <span>you</span>r account </h2>
                <div class="formArea loginFormArea">
                    <form method="POST" action="{{ route('login') }}" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                           name="email" id="email" value="{{ old('email') }}"
                                           autocomplete="email" placeholder="Enter Your Email*"
                                           autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror" name="password"
                                           required autocomplete="current-password" placeholder="Password*">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="checkbox" name="remember"
                                           id="remember" {{ old('remember') ? 'checked' : '' }} value="1">
                                    &nbsp; {{ __('Remember Me') }}?
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <button type="submit" class="btn btnMain">Sign In</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group forgotArea">

                                    @if (Route::has('password.request'))
                                        <a href="{{ route('password.request') }}"> {{ __('Forgot Password?') }}</a>
                                        <br>
                                    @endif

                                    <p>Don't have an account? <a
                                            href="{{ route('register') }}"> {{ __('Register') }}</a></p>

                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

        $('#phone_number').focusout(function () {
            if (/^(03)\d{9}$/.test($(this).val())) {
                // value is ok, use it
            } else {

            }


        });
    </script>
@endpush
