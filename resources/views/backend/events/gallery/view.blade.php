@extends('layouts.master')
@section('title','Event Gallery')
@push('css')
    <style>
        #comments {
            width: 95%;
            height: 100px;
        }

        .commentLabel {
            float: left;
            margin-left: 11px;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ ucwords($event->title) }}
                        </h3>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr No.</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($event->galleries))
                        @foreach($event->galleries as $gallery)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($gallery->image) ? '' : 'none'}}; max-width: 100%"
                                         id="img"
                                         src="{{ $gallery->image_path}}"
                                         alt="your image"/></td>
                                <td nowrap style="width: 12%;">
                                    <form method="post"
                                          action="{{ route('admin.event.gallery.single.destroy', $gallery->id) }}"
                                          id="delete_{{ $gallery->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$gallery->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [2]}
            ],
        });

    </script>
@endpush
