@extends('layouts.master')
@section('title','Event Gallery')
@push('css')
    <style>

        .m-portlet textarea {
            height: 100% !important;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">


@endpush
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Event Gallery') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form method="post" action="{{route('admin.event.gallery.store')}}"
                              enctype="multipart/form-data"
                              class="dropzone m-form" id="dropzone">
                            <input type="hidden" name="event_id" value="{{$event_id}}">
                            @csrf
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script type="text/javascript">

        Dropzone.options.dropzone =
            {
                addRemoveLinks: true,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                maxFiles: 20,
                maxFilesize: 1,//max file size in MB,
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time + file.name;
                },
                init: function () {
                    this.on("error", function (file, message) {
                        toastr.success(message, 'Error Alert', {
                            timeOut: 5000
                        });
                        this.removeFile(file);
                    });
                },
                removedfile: function (file) {
                    var name = file.upload.filename;
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        type: 'POST',
                        url: '{{ route('admin.event.gallery.destroy') }}',
                        data: {filename: name},
                        success: function (data) {
                            toastr.success('File has been successfully removed!!', 'Success Alert', {
                                timeOut: 5000
                            });
                        },
                        error: function (e) {
                            toastr.success(e, 'Error Alert', {
                                timeOut: 5000
                            });
                        }
                    });
                    var fileRef;
                    return (fileRef = file.previewElement) != null ?
                        fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
                success: function (file, response) {
                    toastr.success('File has been successfully Added!!', 'Success Alert', {
                        timeOut: 5000
                    });
                },
                error: function (file, response) {
                    toastr.success('Something Went Wrong', 'Error Alert', {
                        timeOut: 5000
                    });
                }
            };
    </script>
@endpush
