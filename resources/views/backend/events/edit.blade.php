@extends('layouts.master')
@section('title','Events')
@push('css')
    <style>
        .modal .modal-content .modal-header {

            background: #f1743b !important;
        }

        .modal .modal-content .modal-header .modal-title {
            color: #fff !important;
        }


        .button-right {
            float: right;
            margin-top: 12px;
        }
       .m-portlet textarea{
            height: 100% !important;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Event') }}
                        </h3>
                    </div>
                </div>

            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.events.update',$event->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title',$event->title) }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="status"
                                                    class="form-control @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status">
                                                <option value="" {{ (old('status') == '') ? 'selected' : '' }}>Select an
                                                    option
                                                </option>
                                                <option
                                                    value="past" {{ (old('status',$event->status) == 'past') ? 'selected' : '' }}>
                                                    Past
                                                </option>
                                                <option
                                                    value="upcoming" {{ (old('status',$event->status) == 'upcoming') ? 'selected' : '' }}>
                                                    Upcoming
                                                </option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address" value="{{ old('address',$event->address) }}"
                                                   autocomplete="address" autofocus>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="event_date"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Event Date') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="event_date" type="date"
                                                   class="form-control @error('event_date') is-invalid @enderror"
                                                   name="event_date" value="{{ old('event_date',$event->event_date) }}"
                                                   autocomplete="event_date" autofocus>

                                            @error('event_date')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="start_time"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Start Time') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="start_time" type="time"
                                                   class="form-control @error('start_time') is-invalid @enderror"
                                                   name="start_time" value="{{ old('start_time',$event->start_time) }}"
                                                   autocomplete="start_time" autofocus>

                                            @error('start_time')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="end_time"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('End Time') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="end_time" type="time"
                                                   class="form-control @error('end_time') is-invalid @enderror"
                                                   name="end_time" value="{{ old('end_time',$event->end_time) }}"
                                                   autocomplete="end_time" autofocus>

                                            @error('end_time')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="description"><strong>Description </strong><span
                                                    class="mandatorySign">*</span></label>
                                            <textarea name="description" id="description"
                                                      rows="10" class="@error('description') is-invalid @enderror"
                                                      cols="80"
                                                      style="
                                                      margin-top: 0px;
                                                      margin-bottom: 0px;
                                                      height: 147px !important;"
                                            >{{ old('description',$event->description) }}</textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}</label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($event->image) ? 'block' : 'none'}};"
                                                 id="img"
                                                 src="{{ asset('/uploads/events/'.$event->image) }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
{{--                                        <div class="col-md-6">--}}
{{--                                            <label for="location"--}}
{{--                                                   class="col-md-4 col-form-label text-md-left">{{ __('Location') }}--}}
{{--                                                <span class="mandatorySign">*</span></label>--}}
{{--                                            <input id="location" type="text"--}}
{{--                                                   class="form-control @error('location') is-invalid @enderror"--}}
{{--                                                   name="location" value="{{ old('location',$event->location) }}"--}}
{{--                                                   autocomplete="location" autofocus>--}}

{{--                                            @error('location')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
                                    </div>

                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.events.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('models')


@endpush

@push('js')


{{--    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>--}}
{{--    <script>--}}
{{--        CKEDITOR.replace('description');--}}
{{--    </script>--}}
@endpush
