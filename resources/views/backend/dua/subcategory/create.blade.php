@extends('layouts.master')
@section('title','Duas Chapter')
@push('css')
    <style>

        .select2-container {
            width: 100% !important;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Chapter') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.dua-sub-categories.store') }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">


                                        <div class="col-md-6">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title') }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Dua') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control categories @error('category_id') is-invalid @enderror"
                                                    name="category_id" autocomplete="category_id">
                                                <option value="">Select Category</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{$category->id}}">{{ucfirst($category->title)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-4">
                                            <label for="description_en"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('English Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description_en"
                                                      class="form-control @error('description_en') is-invalid @enderror"
                                                      name="description_en"
                                                      autocomplete="description_en">{{ old('description_en') }}</textarea>

                                            @error('description_en')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="description_ar"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Arabic Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description_ar"
                                                      class="form-control @error('description_ar') is-invalid @enderror"
                                                      name="description_ar"
                                                      autocomplete="description_ar">{{ old('description_ar') }}</textarea>

                                            @error('description_ar')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="description_ur"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Urdu Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description_ur"
                                                      class="form-control @error('description_ur') is-invalid @enderror"
                                                      name="description_ur"
                                                      autocomplete="description_ur">{{ old('description_ur') }}</textarea>

                                            @error('description_ur')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">


                                        <div class="col-md-4">
                                            <label for="audio_en"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('English Audio Link') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="audio_en" type="text"
                                                   class="form-control @error('audio_en') is-invalid @enderror"
                                                   name="audio_en" value="{{ old('audio_en') }}"
                                                   autocomplete="audio_en" autofocus>

                                            @error('audio_en')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="audio_ar"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Arabic Audio Link') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="audio_ar" type="text"
                                                   class="form-control @error('audio_ar') is-invalid @enderror"
                                                   name="audio_ar" value="{{ old('audio_ar') }}"
                                                   autocomplete="audio_ar" autofocus>

                                            @error('audio_ar')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="audio_ur"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Urdu Audio Link') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="audio_ur" type="text"
                                                   class="form-control @error('audio_ur') is-invalid @enderror"
                                                   name="audio_ur" value="{{ old('audio_ur') }}"
                                                   autocomplete="audio_ur" autofocus>

                                            @error('audio_ur')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.dua-sub-categories.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $('.categories').select2();
    </script>
@endpush
