@extends('layouts.master')
@section('title','Duas')
@push('css')
    <style>
        #comments {
            width: 95%;
            height: 100px;
        }

        .commentLabel {
            float: left;
            margin-left: 11px;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Dua
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.dua-categories.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Dua</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr NO.</th>
                        <th>Name</th>
                        <th>Chapters</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($categories))
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($category->title)}}</td>
                                <td>

                                    @if(!empty($category->subcategories))
                                        @foreach($category->subcategories as $subcategory)
                                            <ul>
                                                <li>{{ ucwords($subcategory->title) }}</li>
                                            </ul>
                                        @endforeach
                                    @endif
                                </td>
                                <td nowrap style="width: 20%;">
                                    <a href="{{route('admin.dua-categories.edit',$category->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post"
                                          action="{{ route('admin.dua-categories.destroy', $category->id) }}"
                                          id="delete_{{ $category->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$category->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [3]}
            ],
        });


    </script>
@endpush
