@extends('layouts.master')
@section('title','Duas Chapter')
@push('css')
    <style>
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Video') }}
                        </h3>
                    </div>
                </div>

            </div>


            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.videos.update',$subcategory->id) }}"
                              id="create"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group row">


                                        <div class="col-md-6">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title',$subcategory->title) }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Category') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control categories @error('category_id') is-invalid @enderror"
                                                    name="category_id" autocomplete="category_id">
                                                <option value="">Select Category</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{$category->id}}" {{ $subcategory->momin_tv_category_id == $category->id ? 'selected' : '' }}>{{ucfirst($category->name)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="short_description"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Short Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="short_description"
                                                      class="form-control @error('short_description') is-invalid @enderror"
                                                      name="short_description"
                                                      autocomplete="short_description">{{ old('short_description',$subcategory->short_description) }}</textarea>

                                            @error('short_description')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="link"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Link') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="link" type="text"
                                                   class="form-control @error('link') is-invalid @enderror"
                                                   name="link" value="{{ old('link',$subcategory->link) }}"
                                                   autocomplete="link" autofocus>

                                            @error('link')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}</label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($subcategory->image) ? 'block' : 'none'}};"
                                                 id="img"
                                                 src="{{ asset('/uploads/video_thumbnail/'.$subcategory->image) }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.videos.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('models')


@endpush

@push('js')

    <script>
        $('.categories').select2();
    </script>

@endpush
