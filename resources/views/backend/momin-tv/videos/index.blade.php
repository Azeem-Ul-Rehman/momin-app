@extends('layouts.master')
@section('title','Momin TV Videos')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Videos
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.videos.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Videos</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr No</th>
                        <th> Name</th>
                        <th> Short Description</th>
                        <th> Link</th>
                        <th> Category</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($subCategories))
                        @foreach($subCategories as $subCategory)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ucfirst($subCategory->title)}}</td>
                                    <td>{{ucfirst($subCategory->short_description)}}</td>
                                    <td>{{$subCategory->link}}</td>
                                    <td>{{$subCategory->category->name}}</td>
                                    <td nowrap style="width: 12%;">
                                        <a href="{{route('admin.videos.edit',$subCategory->id)}}"
                                           class="btn btn-sm btn-info pull-left ">Edit</a>
                                        <form method="post" action="{{ route('admin.videos.destroy', $subCategory->id) }}"
                                              id="delete_{{ $subCategory->id }}">
                                            @csrf
                                            @method('DELETE')
                                            <a style="margin-left:10px;" class="btn btn-sm btn-danger m-left"
                                               href="javascript:void(0)"
                                               onclick="if(confirmDelete()){ document.getElementById('delete_<?=$subCategory->id?>').submit(); }">
                                                Delete
                                            </a>
                                        </form>
                                    </td>
                                </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [5]}
            ],
        });

    </script>
@endpush
