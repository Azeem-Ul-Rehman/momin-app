@extends('layouts.master')
@section('title','Hadees')
@push('css')
    <style>
        #comments {
            width: 95%;
            height: 100px;
        }

        .commentLabel {
            float: left;
            margin-left: 11px;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Hadees Books
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.hadees.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Hadees Book</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr No.</th>
                        <th>Title</th>
                        <th style="width: 10%;">Image</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($hadees))
                        @foreach($hadees as $hd)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($hd->title)}}</td>
                                <td>
                                    <img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($hd->icon) ? '' : 'none'}};
                                         object-fit: cover;"
                                         id="img"
                                         src="{{ asset('/uploads/hadees/'.$hd->icon) }}"
                                         alt="{{$hd->title}}"/>
                                </td>
                                <td nowrap style="width: 40%;">
                                    <a href="{{route('admin.hadees.edit',$hd->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post" action="{{ route('admin.hadees.destroy', $hd->id) }}"
                                          id="delete_{{ $hd->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$hd->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [3]}
            ],
        });


    </script>
@endpush
