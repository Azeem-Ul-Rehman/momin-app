@extends('layouts.master')
@section('title','Banners')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Banner') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.banner.update',$banner->id) }}"
                              enctype="multipart/form-data" role="form">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="short_summary"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Short Summary') }} <span class="mandatorySign">*</span></label>
                                            <input id="short_summary" type="text"
                                                   class="form-control @error('short_summary') is-invalid @enderror"
                                                   name="short_summary" value="{{ old('short_summary',$banner->short_summary) }}"
                                                   autocomplete="short_summary" autofocus>

                                            @error('short_summary')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="quoted_by"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Quoted By') }} <span class="mandatorySign">*</span></label>
                                            <input id="quoted_by" type="text"
                                                   class="form-control @error('quoted_by') is-invalid @enderror"
                                                   name="quoted_by" value="{{ old('quoted_by',$banner->quoted_by) }}"
                                                   autocomplete="quoted_by" autofocus>

                                            @error('quoted_by')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">

                                            <label for="image"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Image (1920 * 1004)') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)"
                                                   name="image" style="padding: 9px; cursor: pointer" id="image"
                                                   value="{{$banner->image}}">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($banner->image) ? 'block' : 'none'}};"
                                                 id="img"
                                                 src="{{$banner->image_path}}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.banner.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
