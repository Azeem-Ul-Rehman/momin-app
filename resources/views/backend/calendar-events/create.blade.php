@extends('layouts.master')
@section('title','Calendar Events')
@push('css')
    <style>

        .m-portlet textarea {
            height: 100% !important;
        }
    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Calendar Event') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.calendar-events.store') }}" id="create"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title') }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="english_date"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('English Date') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="english_date" type="date"
                                                   class="form-control @error('english_date') is-invalid @enderror"
                                                   name="english_date" value="{{ old('english_date') }}"
                                                   autocomplete="english_date" autofocus>

                                            @error('english_date')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="islamic_date"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Islamic Date') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="islamic_date" type="text"
                                                   class="form-control @error('islamic_date') is-invalid @enderror"
                                                   name="islamic_date" value="{{ old('islamic_date') }}"
                                                   autocomplete="islamic_date" autofocus>

                                            @error('islamic_date')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.calendar-events.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
