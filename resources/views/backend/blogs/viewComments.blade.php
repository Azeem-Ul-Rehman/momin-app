@extends('layouts.master')
@section('title','Blog Comments')
@push('css')
    <style>
        #comments {
            width: 95%;
            height: 100px;
        }

        .commentLabel {
            float: left;
            margin-left: 11px;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ ucwords($blog->title) }}
                        </h3>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr NO.</th>
                        <th>User</th>
                        <th style="width: 40%;">Comments</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($blog->comments))
                        @foreach($blog->comments as $comments)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($comments->user->profile_pic) ? '' : 'none'}};
                                             border-radius: 50% !important;
                                             max-width: 15%;"
                                         id="img"
                                         src="{{ asset('/uploads/user_profiles/'.$comments->user->profile_pic) }}"
                                         alt="your image"/> {{ $comments->user->fullName() }}</td>
                                <td>{{$comments->comments}}</td>

                                <td><select id="status"
                                            class="form-control comment_status @error('status') is-invalid @enderror"
                                            name="status" autocomplete="status"
                                            data-comment-id="{{$comments->id}}">
                                        <option value="">Select an option</option>
                                        <option
                                            value="active" {{ $comments->status == 'active' ? 'selected': '' }}>
                                            Active
                                        </option>
                                        <option
                                            value="in-active" {{ $comments->status == 'in-active' ? 'selected': '' }}>
                                            In Active
                                        </option>

                                    </select></td>
                                <td nowrap style="width: 12%;">
                                    <form method="post" action="{{ route('admin.blogs.comment.destroy', $comments->id) }}"
                                          id="delete_{{ $comments->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$comments->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [4]}
            ],
        });
        $(document).on('change', '.comment_status', function () {

            form = $(this).closest('form');
            node = $(this);
            var status = $(this).val();
            var id = $(this).data('comment-id');
            var request = {"status": status, "id": id};
            if (status !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('admin.update.blog.comment.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status === "success") {
                            alert("Status Updated Successfully");

                        }
                    },
                    error: function () {
                        alert("Something Went Wrong");

                    }
                });
            } else {
                alert("Please Select Status");

            }

        });

    </script>
@endpush
