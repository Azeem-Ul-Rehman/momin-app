@extends('layouts.master')
@section('title','Blogs')
@push('css')
    <style>
        #comments{
            width: 95%;
            height: 100px;
        }
        .commentLabel{
            float: left;
            margin-left: 11px;
            margin-top: 12px;
        }
    </style>
@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Blogs
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.blogs.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Blog</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th>Sr NO.</th>
                        <th>Title</th>
                        <th>Likes</th>
                        <th>Comments</th>
                        <th style="width: 10%;">Image</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($blogs))
                        @foreach($blogs as $blog)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($blog->title)}}</td>
                                <td>{{count($blog->likes)  }}</td>
                                <td>{{count($blog->comments)}}</td>
                                <td>
                                    <img width="300" height="200" class="img-thumbnail"
                                         style="display:{{($blog->image) ? '' : 'none'}};
                                             /*height: 20%;*/
                                             object-fit: cover;"
                                         id="img"
                                         src="{{ asset('/uploads/blogs/'.$blog->image) }}"
                                         alt="your image"/>
                                </td>
                                <td>{{ucwords(str_replace('-',' ',$blog->status))}}</td>
                                <td nowrap style="width: 40%;">
                                    <a href="javascript:void(0)" onclick="addComment({{$blog->id}})"
                                       class="btn btn-sm btn-info pull-left">Add Comment</a>
                                    <a href="{{route('admin.view.blogs.comments',$blog->id)}}"
                                       class="btn btn-sm btn-info pull-left ">View Comments</a>
                                    <a href="{{route('admin.blogs.edit',$blog->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                    <form method="post" action="{{ route('admin.blogs.destroy', $blog->id) }}"
                                          id="delete_{{ $blog->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <a class="btn btn-sm btn-danger m-left"
                                           href="javascript:void(0)"
                                           onclick="if(confirmDelete()){ document.getElementById('delete_<?=$blog->id?>').submit(); }">
                                            Delete
                                        </a>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="addComments" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Comment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-12 col-xs-12 col-sm-12">
                            <form action="{{ route('admin.add.blogs.comments') }}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <input type="hidden" name="user_id" id="user_id" value="{{auth()->id()}}">
                                    <input type="hidden" name="blog_id" id="blog_id" value="">
                                </div>

                                <div class="form-group row" style="    margin-top: -7rem;">
                                    <div class="col-md-12">
                                        <label for="comments" class="commentLabel"><strong>Comment </strong><span
                                                class="mandatorySign">*</span></label>
                                    </div>
                                    <div class="col-md-12">

                                        <textarea name="comments" id="comments">{{ old('comments') }}</textarea>

                                        @error('comments')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>

                                <button type="submit"
                                        class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air button-right">
                                    SAVE
                                </button>
                            </form>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [6]}
            ],
        });

        function addComment(id) {

            $('#blog_id').val(id);
            $('#addComments').modal('show');
        }

    </script>
@endpush
