@extends('frontend.layout.app')
@push('header')



    <!-- link to the SqPaymentForm library -->
    <script type="text/javascript" src="https://js.squareupsandbox.com/v2/paymentform"></script>

    <!-- link to the local SqPaymentForm initialization -->
    <script type="text/javascript" src="{{ asset('js/sqpaymentform.js') }}">
    </script>

    <!-- link to the custom styles for SqPaymentForm -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sqpaymentform-basic.css') }}">
    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            if (SqPaymentForm.isSupportedBrowser()) {
                paymentForm.build();
                // paymentForm.recalculateSize();
            }
        });
    </script>

@endpush
@push('css')
@endpush
@section('content')

    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Package payment</h3>
            </div>
        </div>
    </div>
    <div class="packagesSection packagePayment">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="packageDetails">
                        <h3><span>Pa</span>ckages Details</h3>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
                            <tr>
                                <th>Package payment</th>
                                <th><span>{{ $package->name }}</span></th>
                            </tr>
                            <tr>
                                <th>Package price</th>
                                <th><span>${{ $package->price }}</span></th>
                            </tr>
                            <tr>
                                <th>Background Checks:</th>
                                <th><span id="BackgroundChecks">{{ $subscription->background_checks }}</span></th>
                            </tr>
                            <tr>
                                <th>Addon price</th>
                                <th><span>${{ $package->addon_price }}</span></th>
                                <input type="hidden" value="{{ $package->addon_price }}" id="addon_price">
                            </tr>
                            <tr>
                                <th>Addon Checks:</th>
                                <th><span id="AddOnChecks">   <input type="number"
                                                                     value="{{ $subscription->addon_checks }}"
                                                                     id="addnewaddon"
                                                                     style="width: 80px;     padding-right: 5px;">
                                        <button type="button" id="UpdateAddOn"><i
                                                class="fa fa-edit"></i></button></span></th>
                            </tr>
                            <tr>
                                <th>Total Checks:</th>
                                <th><span
                                        id="TotalChecks">{{ $subscription->addon_checks + $subscription->background_checks }}</span>
                                </th>
                            </tr>
                            <tr>
                                <th>Total Price:</th>
                                <th><span id="TotalPrice">${{ $subscription->price }}</span></th>
                            </tr>
                        </table>

                        <a href="{{ route('realtor.subscribe.package') }}" class="btn btnMain">Edit package</a>
                    </div>
                </div>
                <div class="col-md-6 col-md-offset-2 col-sm-7 col-xs-12">
                    <h3><span>En</span>ter Payment details</h3>
                    <div class="paymentForm">
                        <div id="form-container">
                            <div id="sq-ccbox">
                                <!--
                                  Be sure to replace the action attribute of the form with the path of
                                  the Transaction API charge endpoint URL you want to POST the nonce to
                                  (for example, "/process-card")
                                -->
                                <form id="nonce-form" novalidate action="{{ route('customer.charge') }}" method="post">
                                @csrf
                                <!--
                             After a nonce is generated it will be assigned to this hidden input field.
                         -->
                                    <input type="hidden" id="amount" name="amount" value="{{$subscription->price}}">
                                    <input type="hidden" id="card-nonce" name="nonce">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <span class="label">Card Number</span>
                                                    <div id="sq-card-number"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">

                                                    <span class="label">Expiration</span>
                                                    <div id="sq-expiration-date"></div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">

                                                    <span class="label">CVV</span>
                                                    <div id="sq-cvv"></div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">

                                                    <span class="label">Postal</span>
                                                    <div id="sq-postal-code"></div>

                                                </div>
                                            </div>
                                        </div>


                                    </fieldset>

                                    <button id="sq-creditcard" class="btn btnMain" onclick="requestCardNonce(event)">
                                        Pay
                                        ${{$subscription->price}}
                                    </button>

                                    <div id="error"></div>


                                </form>
                            </div> <!-- end #sq-ccbox -->

                        </div> <!-- end #form-container -->
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
@push('models')
@endpush
@push('js')
    <script>
        var id = "{{ $subscription->id }}";
        $("#UpdateAddOn").click(function () {

            $.ajax({
                type: "POST",
                url: "{{ route('ajax.update.addon.checks') }}",
                data: {Id: id, newAddOn: $("#addnewaddon").val(), _token: "{{ csrf_token() }}"},
                success: function (response) {
                    toastr[response.status](response.message);
                    window.location.reload();
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });
    </script>
@endpush

