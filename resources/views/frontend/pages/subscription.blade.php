@extends('frontend.layout.app')
@push('css')
@endpush
@section('content')

    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Subscribe Package</h3>
            </div>
        </div>
    </div>
    <div class="packagesSection">
        <div class="container">

            <form id="subscribe" action="{{ route('package.subscribe') }}" method="POST">
                @csrf
                <input type="hidden" name="realtor_id" value="{{ $user->realtor->id }}">
                <input type="hidden" name="text_page" value="{{ $textPage }}">
                <div class="row">

                    @if(!empty($packages) && count($packages) > 0)
                        @foreach($packages as $package)
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="packageMain text-center">
                                    <div class="packageHeader">
                                        <input type="radio" name="package_id" value="{{$package->id}}"
                                               onclick="packagePrice('{{$package->price}}','{{$package->id}}')"
                                               id="package_id">
                                        <label>{{ ucwords($package->name)}}</label>
                                    </div>
                                    <div class="packageBody">
                                        <h3>$ <span id="price">{{ $package->price }}</span></h3>
                                        <h4>Background Checks: <span
                                                id="background_check">{{ $package->background_checks }}</span>
                                        </h4>
                                        @if(strtolower($package->name) == 'free')
                                            <p>Add-on: upgrade to paid plan</p>
                                        @else
                                            <button type="button" class="btn signupBtn hidebtn" id="addon-screen{{$package->id}}"
                                               onclick="openInputField('{{$package->id}}')" disabled>
                                                Add-on: $<span id="addon_price">{{ $package->addon_price }}</span>/check</button>
                                        @endif
                                        <br>
                                        <br>
                                        <div class="row hideInput" id="add_more_check{{$package->id}}" hidden>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <span>Add More Checks</span>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                <input type="number" min="0"
                                                       name="add_more_check_count[{{$package->id}}]"
                                                       id="add_more_check_count{{$package->id}}"
                                                       data-price="{{$package->price}}"
                                                       data-addon-price="{{$package->addon_price}}"
                                                       onchange="addMoreCheck('{{$package->id}}')">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
                <div class="row">
                    <div class="col-md-12 text-center" style="margin-top: 40px">
                        <span>Total: $<span id="total_price">0</span></span>
                        <input type="hidden" name="total" id="total" value="0">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">

                        <button type="submit" class="btn btnMain" id="disabledBtn" disabled>Subscribe</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--    <div class="aboutUs">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-md-6 col-sm-5 col-xs-12"></div>--}}
    {{--                <div class="col-md-6 col-sm-7 col-xs-12">--}}
    {{--                    <h2 class="heading"><span>Subscribe Package</span></h2>--}}
    {{--                    <form id="subscribe" action="{{ route('package.subscribe') }}" method="POST">--}}
    {{--                        @csrf--}}
    {{--                        <input type="hidden" name="realtor_id" value="{{ $user->realtor->id }}">--}}
    {{--                        <input type="hidden" name="text_page" value="{{ $textPage }}">--}}
    {{--                        @if(!empty($packages) && count($packages) > 0)--}}

    {{--                            <div class="row">--}}
    {{--                                @foreach($packages as $package)--}}
    {{--                                    <div class="col-md-6">--}}

    {{--                                        <p><input type="radio" name="package_id" value="{{$package->id}}"--}}
    {{--                                                  onclick="packagePrice('{{$package->price}}')"--}}
    {{--                                                  id="package_id"> {{ ucwords($package->name) }}</p>--}}
    {{--                                        <p>$ <span id="price">{{ $package->price }}</span> /=</p>--}}
    {{--                                        <p>Background Checks: <span--}}
    {{--                                                id="background_check">{{ $package->background_checks }}</span></p>--}}
    {{--                                        @if(strtolower($package->name) == 'free')--}}
    {{--                                            <span id="addon-screen">Add-on : Upgrade to paid plan</span>--}}
    {{--                                        @else--}}
    {{--                                            <a href="javascript:void(0)" class="btn btn-info" id="addon-screen"--}}
    {{--                                               onclick="openInputField('{{$package->id}}')">Add-on: $<span--}}
    {{--                                                    id="addon_price">{{ $package->addon_price }}</span>/check</a>--}}
    {{--                                        @endif--}}

    {{--                                    </div>--}}
    {{--                                    <span id="add_more_check{{$package->id}}" style="display: none">Add More Checks:--}}
    {{--                                        <input type="number" min="0" name="add_more_check_count[{{$package->id}}]" id="add_more_check_count{{$package->id}}" data-price="{{$package->price}}" data-addon-price ="{{$package->addon_price}}" onchange="addMoreCheck('{{$package->id}}')">--}}
    {{--                                    </span>--}}
    {{--                                @endforeach--}}

    {{--                            </div>--}}
    {{--                            <br>--}}
    {{--                            <br>--}}
    {{--                            <br>--}}


    {{--                            <span>Total: $<span id="total_price">0</span></span>--}}
    {{--                            <input type="hidden" name="total" id="total" value="0">--}}



    {{--                        @endif--}}

    {{--                        <button type="submit" class="btn btn-primary" style="float: right" id="disabledBtn" disabled>Subscribe</button>--}}
    {{--                    </form>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


@endsection
@push('models')
@endpush
@push('js')
    <script>
    var messageShow = "{{$messageShow}}";
    if(messageShow == true){
        if(confirm('Your package will be changed and your Remaining Background checks will be added in your current Package.')){

        }else{
            window.location.href ='{{ route('realtor.subscribe.history.index') }}';
        }
        // toastr.error("Your Package will be change");
    }else{

    }
        function openInputField(packageId) {
            var response = false;
            $('input[type="radio"]').each(function () {
                if (($(this).is(':checked')) === true) {
                    response = true;
                }
            });
            debugger;
            if (response === true) {
                $('#add_more_check' + packageId).prop('hidden',false);
            } else {
                toastr.error("Please select a package");
            }


        }

        function packagePrice(price,packageId) {
            $('#total').val(parseInt(price));
            $('#total_price').text(parseInt(price));
            $('.hidebtn').prop('disabled',true);
            $('#addon-screen'+ packageId).prop('disabled',false);
            $('.hideInput').prop('hidden',true);
        }

        function addMoreCheck(packageId) {
            var package_price = $('#add_more_check_count' + packageId).data('price');
            var addon_price = $('#add_more_check_count' + packageId).data('addon-price');
            var background_check = $('#add_more_check_count' + packageId).val();

            var total_price = parseInt(package_price) + (parseFloat(addon_price) * parseInt(background_check));
            $('#total').val((total_price));
            $('#total_price').text((total_price));
            $('#disabledBtn').prop("disabled", false);
        }

        $('input[type="radio"]').on('click', function () {
            $('#disabledBtn').prop("disabled", false);
        });


    </script>

@endpush
