<div class="howItWorks" id="works">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="row">
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how1.png') }}" alt="">
                        <p>1. Register</p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how2.png') }}" alt="">
                        <p>2. Set up Payments </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how3.png') }}" alt="">
                        <p>3. Download the app
                            Accept permissions
                            Accept disclaimer
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center howImg howDesktop">
                <img src="{{ asset('frontend/images/howImg.png') }}" alt="">
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="row">
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how4.png') }}" alt="">
                        <p>4. Set your
                            appointments
                        </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how5.png') }}" alt="">
                        <p>5. Check
                            Background </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how6.png') }}" alt="">
                        <p>6. Secure Your
                            Showings
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center howImg howMobile">
                <img src="{{ asset('frontend/images/howImg.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
