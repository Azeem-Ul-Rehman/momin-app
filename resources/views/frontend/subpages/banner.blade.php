<div class="banner" id="home">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bannerInner">
                    <h2>SECURE <span>SOLUTIONS</span></h2>
                    <p>For Realtors to provide safety in the field</p>
                    <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <a href="#"><img src="{{ asset('frontend/images/appStore.png') }}" alt=""></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <a href="#"><img src="{{ asset('frontend/images/playStore.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 bannerPhone">
                <img src="{{ asset('frontend/images/phone.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
