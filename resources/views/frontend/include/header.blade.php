@php
    $settings =\App\Models\Setting::all();



@endphp


<div class="navbar-fixed-top">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}"
                                                                         alt="MOMIN APP" ></a></div>
            <div class="collapse navbar-collapse" id="defaultNavbar1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#about">about us </a></li>
                    <li><a href="#works">how it works</a></li>
                    <li><a href="#contact">contact</a></li>
                    @guest
                        <li><a href="{{ route('login') }}" class="signupBtn">Login</a></li>
                        <li><a href="{{ route('register') }}" class="signupBtn">Sign Up</a></li>
                    @endguest
                    @auth
                        <li>
                            <div class="col-md-6 col-sm-6 col-xs-5 topbarRight  hidden-xs">

                                <!-- Example single danger button -->
                                <div class="btn-group">
                                    <button type="button" class="btn buttonMain hvr-bounce-to-right dropdown-toggle" style="margin-top: 15px;background-color: transparent"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Welcome {{ strtoupper(auth()->user()->fullName()) }} <span class="caret"></span>
                                    </button>


                                    <div class="dropdown-menu">
                                        <ul class="nav navbar-nav">
                                            @if(auth()->user()->hasRole('admin'))
                                                <li><a class="dropdown-item"
                                                       href="{{ route('admin.dashboard.index') }}">Dashboard</a></li>
                                            @elseif(auth()->user()->hasRole('realtor'))
                                                <li><a class="dropdown-item"
                                                       href="{{ route('realtor.dashboard.index') }}">Dashbaord</a></li>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            <li><a href="{{ route('logout') }}" class="dropdown-item"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                            </li>
                                        </ul>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </li>

                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</div>
