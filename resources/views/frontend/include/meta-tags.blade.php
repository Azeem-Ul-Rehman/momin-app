@if(is_null($meta_information))
    <title>MOMIN APP | @yield('title')</title>
    <meta name="description"
          content="MOMIN APP">
    <meta name="keywords" content="MOMIN APP">
@else
    <title>MOMIN APP | {{$meta_information->title}}</title>
    <meta name="description" content="{{ $meta_information->description }}">
    <meta name="keywords" content="{{ $meta_information->keywords }}">
@endif


