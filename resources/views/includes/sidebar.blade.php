<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            <li class="m-menu__item {{ (request()->is('admin/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.users.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-user-add"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Users</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/aal-e-muhammad') || request()->is('admin/aal-e-muhammad/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.aal-e-muhammad.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-comment"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Aal-e-Muhammad</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('admin/blogs') || request()->is('admin/blogs/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.blogs.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-comment"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Blogs</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('admin/events') || request()->is('admin/events/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.events.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-comment"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Events</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('admin/calendar-events') || request()->is('admin/calendar-events/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.calendar-events.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-event-calendar-symbol"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Calendar Events</span>
											</span></span></a></li>
            <li class="m-menu__item m-menu__item--submenu {{ (request()->is('admin/dua-categories') || request()->is('admin/dua-categories/*') || request()->is('admin/dua-sub-categories') || request()->is('admin/dua-sub-categories/*')) ? 'activeMenu m-menu__item--open' : '' }} "
                aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i
                        class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-text">
												Dua
											</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu" style="">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">

                        <li class="m-menu__item  {{ (request()->is('admin/dua-categories') || request()->is('admin/dua-categories/*')) ? 'activeSubMenuItem' : ''}}"
                            aria-haspopup="true" data-redirect="true"><a
                                href="{{ route('admin.dua-categories.index') }}"
                                class="m-menu__link "><i
                                    class="m-menu__link-icon flaticon-line-graph"></i><span
                                    class="m-menu__link-title"> <span
                                        class="m-menu__link-wrap"> <span class="m-menu__link-text">Categories</span>
											</span></span></a></li>

                        <li class="m-menu__item  {{ (request()->is('admin/dua-sub-categories') || request()->is('admin/dua-sub-categories/*'))? 'activeSubMenuItem' : ''}}"
                            aria-haspopup="true" data-redirect="true"><a
                                href="{{ route('admin.dua-sub-categories.index') }}"
                                class="m-menu__link "><i
                                    class="m-menu__link-icon flaticon-line-graph"></i><span
                                    class="m-menu__link-title"> <span
                                        class="m-menu__link-wrap"> <span class="m-menu__link-text">Sub Categories</span>
											</span></span></a></li>

                    </ul>
                </div>
            </li>
            <li class="m-menu__item m-menu__item--submenu {{ (request()->is('admin/momin-tv') || request()->is('admin/momin-tv/*') || request()->is('admin/videos') || request()->is('admin/videos/*')) ? 'activeMenu m-menu__item--open' : '' }} "
                aria-haspopup="true" data-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i
                        class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-text">
												Momin TV
											</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu" style="">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">

                        <li class="m-menu__item  {{ (request()->is('admin/momin-tv') || request()->is('admin/momin-tv/*')) ? 'activeSubMenuItem' : ''}}"
                            aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.momin-tv.index') }}"
                                                                         class="m-menu__link "><i
                                    class="m-menu__link-icon flaticon-line-graph"></i><span
                                    class="m-menu__link-title"> <span
                                        class="m-menu__link-wrap"> <span class="m-menu__link-text">Categories</span>
											</span></span></a></li>

                        <li class="m-menu__item  {{ (request()->is('admin/videos') || request()->is('admin/videos/*')) ? 'activeSubMenuItem' : ''}}"
                            aria-haspopup="true" data-redirect="true"><a href="{{ route('admin.videos.index') }}"
                                                                         class="m-menu__link "><i
                                    class="m-menu__link-icon flaticon-line-graph"></i><span
                                    class="m-menu__link-title"> <span
                                        class="m-menu__link-wrap"> <span class="m-menu__link-text">Videos</span>
											</span></span></a></li>

                    </ul>
                </div>
            </li>
            <li class="m-menu__item  {{ (request()->is('admin/hadees') || request()->is('admin/hadees/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.hadees.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-book"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Hadees</span>
                                                </span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/banner') || request()->is('admin/banner/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.banner.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-doc"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Banner</span>
                                                </span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/aboutus') || request()->is('admin/aboutus/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.aboutus.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-doc"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">About Us</span>
											</span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/privacypolicy') || request()->is('admin/privacypolicy/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.privacypolicy.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-book"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Privacy Policy</span>
											</span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/settings') || request()->is('admin/settings/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.settings.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-settings"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Settings</span>
											</span></span></a></li>


        </ul>

    </div>

    <!-- END: Aside Menu -->
</div>
