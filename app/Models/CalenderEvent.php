<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalenderEvent extends Model
{
    protected $table = "calender_events";
    protected $guarded = [];
}
