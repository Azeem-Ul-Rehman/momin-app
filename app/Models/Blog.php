<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany(BlogComment::class, 'blog_id', 'id');
    }

    public function likes()
    {
        return $this->hasMany(BlogLike::class, 'blog_id', 'id')->where('status','like');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function getImagePathAttribute()
    {
        return asset('uploads/blogs/' . $this->image);
    }
}
