<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentLike extends Model
{
    protected $table = 'blog_comment_likes';
    protected $guarded = [];
}
