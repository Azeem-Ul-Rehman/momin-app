<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventGallery extends Model
{
    protected $table = 'event_galleries';
    protected $guarded = [];


    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function getImagePathAttribute()
    {
        return asset('images/gallery/' . $this->image);
    }
}
