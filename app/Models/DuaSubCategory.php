<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DuaSubCategory extends Model
{
    protected $table = 'dua_sub_categories';
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(DuaCategory::class, 'category_id', 'id');
    }
}
