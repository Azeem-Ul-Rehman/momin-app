<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $guarded = [];


    public function galleries()
    {
        return $this->hasMany(EventGallery::class, 'event_id', 'id');
    }

    public function goings()
    {
        return $this->hasMany(EventGoing::class, 'event_id', 'id')->where('status','going');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getImagePathAttribute()
    {
        return asset('uploads/events/' . $this->image);
    }
}
