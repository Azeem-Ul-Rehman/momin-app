<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hadees extends Model
{
    protected $table = 'hadees';
    protected $guarded = [];

    public function getImagePathAttribute()
    {
        return asset('uploads/hadees/' . $this->icon);
    }
}
