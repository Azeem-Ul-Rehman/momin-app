<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MominTvVideo extends Model
{
    protected $table = 'momin_tv_videos';
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(MominTvCategory::class,'momin_tv_category_id','id');

    }

    public function getImagePathAttribute()
    {
        return asset('uploads/video_thumbnail/' . $this->image);
    }
}
