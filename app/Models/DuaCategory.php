<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DuaCategory extends Model
{
    protected $table = 'dua_categories';
    protected $guarded = [];

    public function subcategories()
    {
        return $this->hasMany(DuaSubCategory::class, 'category_id', 'id');
    }
}
