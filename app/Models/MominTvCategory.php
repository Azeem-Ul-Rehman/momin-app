<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MominTvCategory extends Model
{
    protected $table = 'momin_tv_categories';
    protected $guarded = [];


    public function videos()
    {
        return $this->hasMany(MominTvVideo::class, 'momin_tv_category_id', 'id')->orderBy('id', 'desc');

    }
}
