<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AaleMuhammad extends Model
{
    protected $table = "aale_muhammads";
    protected $guarded = [];
}
