<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChapterVerse extends Model
{
    protected $table = 'chapter_verses';
    protected $guarded = [];


    public function chapter()
    {
        return $this->belongsTo(Chapter::class, 'chapter_id', 'id');
    }
}
