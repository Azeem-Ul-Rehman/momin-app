<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters';
    protected $guarded = [];

    public function chapterVerses()
    {
        return $this->hasMany(ChapterVerse::class, 'chapter_id', 'id');
    }
}
