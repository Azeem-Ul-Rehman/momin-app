<?php

namespace App\Console\Commands;

use App\OutSourceRequest;
use DateTime;
use Illuminate\Console\Command;

class QuranDataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quran-data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'QuranDataImport';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {


        $date = $this->HijriToJD(6, 28, 1442);

        echo jdtogregorian($date);

//        try {
//            $url = "https://api.quran.com/api/v3/chapters";
//
//            //setting the curl parameters.
//            $ch = curl_init();
//
//            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
//
//
//            $data = curl_exec($ch);
//
////        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//
//
//            if (!curl_errno($ch)) {
//                $info = curl_getinfo($ch);
//
//            }
//
//            curl_close($ch);
//            return ($data);
//        } catch (\Exception $e) {
////            dd($e);
//            return $e->getMessage();
//        }

    }

    function HijriToJD($m, $d, $y)
    {
        return (int)((11 * $y + 3) / 30) + 354 * $y +
            30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
    }


}
