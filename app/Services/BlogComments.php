<?php

namespace App\Services;

use App\Models\BlogComment;
use Illuminate\Support\Facades\Auth;

class BlogComments
{

    public $account_type;
    public $request;


    public function comments($data, $id)
    {
        $blog = BlogComment::create([
            'blog_id' => $data['blog_id'],
            'comments' => $data['comments'],
            'user_id' => $id,
        ]);

        return $blog;
    }

}
