<?php

namespace App\Http\Resources\Event;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"                    => $this->id,
            "title"                 => $this->title ,
            "description"           => $this->description,
//            "location"              => $this->location,
            "address"               => $this->address,
            "event_date"            => date('d M Y', strtotime($this->event_date)),
            "event_day"             => date('d', strtotime($this->event_date)),
            "event_month"           => date('M', strtotime($this->event_date)),
            "event_year"            => date('Y', strtotime($this->event_date)),
            "start_time"            => date("h:i A", strtotime($this->start_time)),
            "end_time"              => date("h:i A", strtotime($this->end_time)),
            "going"                 => count($this->goings),
            "status"                => $this->status,
            "image"                 => $this->image_path,
            "galleries"             => EventGalleryResource::collection($this->galleries),
            "user"                  => new UserResource($this->user),

        ];
    }
}
