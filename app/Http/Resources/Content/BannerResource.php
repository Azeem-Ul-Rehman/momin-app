<?php

namespace App\Http\Resources\Content;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id"                    => $this->id,
            "short_summary"         => $this->short_summary,
            "quoted_by"             => $this->quoted_by,
            "image"                 => $this->image_path,
        ];
    }
}
