<?php

namespace App\Http\Resources\Video;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if(!is_null($this->link)){
            $videoId = explode('v=',$this->link);
            $vidId = $videoId[1];
        }else{
            $vidId = false;
        }



        return [
            "id"                    => $this->id,
            "title"                 => $this->title ,
            "short_description"     => $this->short_description,
            "link"                  => $vidId,
            "image"                 => $this->image_path,
            'category'              => $this->category
        ];
    }
}
