<?php

namespace App\Http\Resources\Video;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class VideoCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->id,
            "name"                  => $this->name ,
            "videos"                => VideoResource::collection($this->videos),
        ];
    }
}
