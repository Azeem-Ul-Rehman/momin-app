<?php

namespace App\Http\Resources\Hadees;

use Illuminate\Http\Resources\Json\JsonResource;


class HadeesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->id,
            "title"                 => $this->title ,
            "source"                => $this->source,
            "icon"                  => $this->image_path,
            "created_at"            => date('d M Y', strtotime($this->created_at)),

        ];
    }
}
