<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResoruce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'mls_id'        => $this->mls_id,
            'website'       => $this->website,
            'pin_number'    => $this->pin_number,
            'address_one'   => $this->address_one,
            'address_two'   => $this->address_two,
            'zip_code'      => $this->zip_code,
            'twitter_id'    => $this->twitter_id,
            'facebook_url'  => $this->facebook_url,
            'paypal_id'     => $this->paypal_id,
            'instagram_id'  => $this->instagram_id,
        ];
    }
}
