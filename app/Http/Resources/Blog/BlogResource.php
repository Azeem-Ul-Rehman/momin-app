<?php

namespace App\Http\Resources\Blog;

use App\Http\Resources\UserResource;
use App\Models\BlogLike;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        $like =BlogLike::where('blog_id', $this->id)->where('user_id', Auth::id())->where('status', 'like')->first();
        return [
            "id"                    => $this->id,
            "title"                 => $this->title ,
            "description"           => $this->description,
            "image"                 => $this->image_path,
            "status"                => $this->status,
            "created_at"            => date('d M Y', strtotime($this->created_at)),
            "totalComments"         => count($this->comments),
            "totalLikes"            => count($this->likes),
            'userLike'              => !is_null($like) ? true : false,
            "comments"              => BlogCommentResource::collection($this->comments),
            "user"                  => new UserResource($this->user),

        ];
    }
}
