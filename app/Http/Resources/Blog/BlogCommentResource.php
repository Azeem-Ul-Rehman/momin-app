<?php

namespace App\Http\Resources\Blog;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->id,
            "comments"              => $this->comments,
            "status"                => $this->status,
            "user"                  => new UserResource($this->user),
        ];
    }
}
