<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        if (isset($this->profile_pic) && !is_null($this->profile_pic)) {
            $profile_pic = $this->profile_pic;
        } else {
            $profile_pic = 'default.png';
        }

        return [
            "id"                    => $this->id,
            "name"                  => $this->first_name . " " . $this->last_name,
            "first_name"            => $this->first_name ?? '',
            "last_name"             => $this->last_name ?? '',
            "username"              => $this->username ?? '',
            "email"                 => $this->email ?? '',
            "phone_number"          => $this->phone_number ?? '',
            "emergency_number"      => $this->emergency_number ?? '',
            "country"               => $this->country ?? '',
            "state"                 => $this->state ?? '',
            "city"                  => $this->city ?? '',
            "language"              => $this->language ?? '',
            "user_type"             => $this->user_type ?? '',
            "status"                => $this->status ?? '',
            "profile_pic"           => asset("/uploads/user_profiles/" . $profile_pic),
        ];
    }
}
