<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Traits\GeneralHelperTrait;
use Illuminate\Support\Facades\Auth;


class RealtorController extends Controller
{
    use GeneralHelperTrait;

    public function notifications()
    {
        try {
            $user = Auth::user();
            $notifications = Notification::where('type_id', $user->realtor->id)->get();


            return response()->json([
                'status' => true,
                'message' => 'Notifications Get Successfully.',
                'data' => $notifications,
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }



}
