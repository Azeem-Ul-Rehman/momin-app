<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Http\Resources\Blog\BlogCommentResource;
use App\Http\Resources\Blog\BlogResource;
use App\Http\Resources\Content\BannerResource;
use App\Http\Resources\Event\EventResource;
use App\Http\Resources\Hadees\HadeesResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\Video\VideoCategoryResource;
use App\Http\Resources\Video\VideoResource;
use App\Models\AaleMuhammad;
use App\Models\Aboutus;
use App\Models\Banner;
use App\Models\Blog;
use App\Models\BlogComment;
use App\Models\BlogLike;
use App\Models\CalenderEvent;
use App\Models\Chapter;
use App\Models\ChapterVerse;
use App\Models\DuaCategory;
use App\Models\DuaSubCategory;
use App\Models\Event;
use App\Models\EventGoing;
use App\Models\Hadees;
use App\Models\MominTvCategory;
use App\Models\MominTvVideo;
use App\Models\State;
use App\Models\City;
use App\Models\Setting;
use App\Models\User;
use App\Services\BlogComments;
use App\Traits\GeneralHelperTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class GeneralController extends Controller
{
    use GeneralHelperTrait;

    protected $blogCommentsService;

    public function __construct(BlogComments $blogCommentsService)
    {
        $this->blogCommentsService = $blogCommentsService;
    }

    // General Sections

    public function states()
    {
        try {
            return response()->json(['data' => State::orderBy('name', 'asc')->get()], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function cities($state_id)
    {
        try {

            return response()->json(['data' => City::where('state_id', $state_id)->orderBy('name', 'asc')->get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function settings()
    {
        try {

            return response()->json(['data' => Setting::get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    // User Profile

    public function updateProfileImage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {

            $user = Auth::user();
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $image->move($destinationPath, $name);
            $profile_image = $name;


            $user->update([
                'profile_pic' => $profile_image,
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Profile image updated successfully.',
                'data' => new UserResource($user),
            ], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'language' => 'required',
            'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'country' => $request->get('country'),
                'state' => $request->get('state'),
                'city' => $request->get('city'),
                'language' => $request->get('language'),
            ]);
            return response()->json([
                'status' => true,
                'message' => 'Profile updated successfully.',
                'data' => new UserResource($user),
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
            'new-password-confirm' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(['status' => false, 'message' => 'Your current password does not matches with the password you provided. Please try again.'], 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(['status' => false, 'message' => 'New Password cannot be same as your current password. Please choose a different password.'], 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(['status' => true, 'message' => 'Password Changed successfully.'], 200);

            } else {
                return response()->json(['status' => false, 'message' => 'New Password must be same as your confirm password.'], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }

    // Get Lat Lng

    public function getLatLong(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'staff_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {
            $user = User::where('id', $request->staff_id)->where('user_type', 'staff')->first();

            if (!is_null($user)) {
                $userInfo = [
                    'staff_id' => $user->id,
                    'latitude' => $user->latitude,
                    'longitude' => $user->longitude,
                ];
                return response()->json(['status' => true, 'message' => '', 'data' => $userInfo], 200);
            } else {
                $userInfo = [];
                return response()->json(['status' => false, 'message' => 'Staff Not Found.', 'data' => $userInfo], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function saveLatLong(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $user_auth = Auth::user();
        try {
            $user = User::where('id', $user_auth->id)->where('user_type', 'staff')->first();
            if (!is_null($user)) {

                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->save();

                $userInfo = [
                    'staff_id' => $user->id,
                    'latitude' => $user->latitude,
                    'longitude' => $user->longitude,
                ];
                return response()->json(['status' => true, 'message' => 'Location Updated Successfully.', 'data' => $userInfo], 200);
            } else {
                $userInfo = [];
                return response()->json(['status' => false, 'message' => 'Staff Not Found.', 'data' => $userInfo], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    // Get Prayer Timings

    public function prayerTimings(Request $request)
    {
        try {
            $prayerTimings = $this->getPrayerTimings($request->timestamp, $request->latitude, $request->longitude, $request->get('method'));
            return response()->json(['data' => $prayerTimings['data']], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    //Get Complete Quran
    public function getQuran($language)
    {
        try {
            $quran = $this->quran($language);
            return response()->json(['data' => $quran], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    // Blogs

    public function getBlogs()
    {
        try {
            $blogs = BlogResource::collection(Blog::all());

            return response()->json(['status' => true, 'data' => $blogs], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function addComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
            'comments' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $this->blogCommentsService->comments($request->all(), Auth::id());
            $blogs = BlogCommentResource::collection(BlogComment::where('blog_id', $request->blog_id)->get());

            return response()->json(['status' => true, 'data' => $blogs], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function deleteComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment_id' => 'required',
            'blog_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $comment = BlogComment::where('id', $request->comment_id)->where('blog_id', $request->blog_id)->first();
            $comment->delete();
            $blogs = BlogCommentResource::collection(BlogComment::where('blog_id', $request->blog_id)->get());

            return response()->json(['status' => true, 'data' => $blogs, 'message' => 'Comment Deleted Successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function blogLike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {

            $checkIsLike = BlogLike::where(['blog_id' => $request->blog_id, 'user_id' => Auth::id()])->first();

            if (is_null($checkIsLike)) {

                $like = new BlogLike();
                $like->blog_id = $request->blog_id;
                $like->user_id = Auth::id();
                $like->status = 'like';
                $like->save();
                $likes = BlogLike::where('blog_id', $request->blog_id)->where('status', 'like')->get();
                return response()->json(['status' => true, 'data' => $like->status, 'count' => count($likes)], 200);
            } else {
                $checkIsLike->status = 'like';
                $checkIsLike->save();
                $likes = BlogLike::where('blog_id', $request->blog_id)->where('status', 'like')->get();
                return response()->json(['status' => true, 'data' => $checkIsLike->status, 'count' => count($likes)], 200);
            }

//            $like = new BlogLike();
//            $like->blog_id = $request->blog_id;
//            $like->user_id = Auth::id();
//            $like->save();
//            $blogs = BlogResource::collection(Blog::all());
//
//            return response()->json(['status' => true, 'data' => $blogs], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function blogUnLike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $like = BlogLike::where(['blog_id' => $request->blog_id, 'user_id' => Auth::id()])->first();
            $like->status = 'not-like';
            $like->save();
            $likes = BlogLike::where('blog_id', $request->blog_id)->where('status', 'like')->get();
            return response()->json(['status' => true, 'data' => $like->status, 'count' => count($likes)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    // Events

    public function getEvents()
    {
        try {
            $upcoming = EventResource::collection(Event::where('status', 'upcoming')->get());
            $past = EventResource::collection(Event::where('status', 'past')->get());

            return response()->json(['status' => true, 'upcoming' => $upcoming, 'past' => $past], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function going(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {

            $checkIsGoing = EventGoing::where(['event_id' => $request->event_id, 'user_id' => Auth::id()])->first();

            if (is_null($checkIsGoing)) {
                $going = new EventGoing();
                $going->event_id = $request->event_id;
                $going->user_id = Auth::id();
                $going->status = 'going';
                $going->save();
                $events = EventGoing::where('event_id', $request->event_id)->where('status', 'going')->get();
                return response()->json(['status' => true, 'data' => $going->status, 'count' => count($events)], 200);
            } else {
                $checkIsGoing->status = 'going';
                $checkIsGoing->save();
                $events = EventGoing::where('event_id', $request->event_id)->where('status', 'going')->get();
                return response()->json(['status' => true, 'data' => $checkIsGoing->status, 'count' => count($events)], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function notGoing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $notGoing = EventGoing::where(['event_id' => $request->event_id, 'user_id' => Auth::id()])->first();
            $notGoing->status = 'not-going';
            $notGoing->save();
            $events = EventGoing::where('event_id', $request->event_id)->where('status', 'going')->get();
            return response()->json(['status' => true, 'data' => $notGoing->status, 'count' => count($events)], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function checkUserIsGoing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }

        try {
            $events = EventGoing::where('event_id', $request->event_id)->where('status', 'going')->get();
            $going = EventGoing::where('event_id', $request->event_id)->where('user_id', Auth::id())->first();
            if (!is_null($going)) {
                return response()->json(['status' => true, 'data' => $going->status, 'count' => count($events)], 200);
            } else {
                return response()->json(['status' => false, 'data' => null, 'count' => 0], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    // Dua

    public function duaCategory()
    {
        try {
            return response()->json(['status' => true, 'data' => DuaCategory::orderBy('title', 'asc')->with('subcategories')->get()], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function duaSubcategory($category_id)
    {
        try {

            return response()->json(['status' => true, 'data' => DuaSubCategory::where('category_id', $category_id)->orderBy('title', 'asc')->with('category')->get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    // Momin TV

    public function mominTvCategory()
    {
        try {
            return response()->json(['status' => true, 'data' => VideoCategoryResource::collection(MominTvCategory::all())], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function mominTvVideos($category_id)
    {
        try {

            return response()->json(['status' => true, 'data' => VideoResource::collection(MominTvVideo::where('momin_tv_category_id', $category_id)->orderBy('title', 'desc')->get())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    // Content Pages

    public function banner()
    {
        try {

            $banner = BannerResource::collection(Banner::orderBy('id', 'desc')->limit(3)->get());
            return response()->json(['status' => true, 'data' => $banner], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function aboutUs()
    {
        try {

            return response()->json(['status' => true, 'data' => Aboutus::first()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getQuranChapters()
    {


        try {

            return response()->json(['status' => true, 'data' => Chapter::all()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

//        $data = $this->quranChapters();
//
//        if (!empty($data['chapters']) && count($data) > 0) {
//            foreach ($data['chapters'] as $key=>$chp) {
//
//                if($chp['id'] == 114){
//                    $chapter = new Chapter();
//                    $chapter->chapter_number    = $chp['chapter_number'];
//                    $chapter->complex_name      = $chp['name_complex'];
//                    $chapter->arabic_name       = $chp['name_arabic'];
//                    $chapter->simple_name       = $chp['name_simple'];
//                    $chapter->verses_count      = $chp['verses_count'];
//                    $chapter->language_name     = $chp['translated_name']['language_name'];
//                    $chapter->type              = $chp['translated_name']['name'];
//                    $chapter->save();
//
//                    if ($chp['verses_count'] > 10) {
//                        $page = ceil($chp['verses_count'] / 10);
//                    } else {
//                        $page = 1;
//                    }
//                    $this->getChapterVerses($chp['id'], $page);
//                }
//
//
//            }
//        }


    }

    public function getVersesByChapter($chapter_id)
    {
        try {

            return response()->json(['status' => true, 'data' => ChapterVerse::where('chapter_id', $chapter_id)->get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getChapterVerses($chapter_id, $page)
    {
        if ($page == 1) {
            $data = $this->chapterVerses($chapter_id, $page);
            if (!empty($data['verses']) && count($data) > 0) {
                foreach ($data['verses'] as $verses) {
                    $this->createChapter($verses);
                }
            }
        } else {
            for ($i = 1; $i <= $page; $i++) {
                $data = $this->chapterVerses($chapter_id, $i);
                if (!empty($data['verses']) && count($data) > 0) {
                    foreach ($data['verses'] as $verses) {
                        $this->createChapter($verses);
                    }
                }
            }
        }


    }

    public function createChapter($verses)
    {
        $chapterVerses = new ChapterVerse();
        $chapterVerses->chapter_id = $verses['chapter_id'];
        $chapterVerses->verse_number = $verses['verse_number'];
        $chapterVerses->verse_key = $verses['verse_key'];
        $chapterVerses->text_madani = $verses['text_madani'];
        $chapterVerses->text_indopak = $verses['text_indopak'];
        $chapterVerses->text_simple = $verses['text_simple'];
        $chapterVerses->text_english = $verses['translations'][0]['text'];
        $chapterVerses->juz_number = $verses['juz_number'];
        $chapterVerses->hizb_number = $verses['hizb_number'];
        $chapterVerses->rub_number = $verses['rub_number'];
        $chapterVerses->sajdah = $verses['sajdah'];
        $chapterVerses->sajdah_number = $verses['sajdah_number'];
        $chapterVerses->page_number = $verses['page_number'];
        $chapterVerses->audio_link = $verses['audio']['url'];
        $chapterVerses->save();
    }


    //Aal-e-Muhammad

    public function getAaleMuhammad()
    {
        try {

            $data = AaleMuhammad::all();
            return response()->json(['status' => true, 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    //Calender Events

    public function calenderEvents()
    {
        try {

            $data = CalenderEvent::all();
            return response()->json(['status' => true, 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    public function familyTree()
    {
        $familyTree = asset('frontend/images/familyTree.png');
        return response()->json(['status' => true, 'data' => $familyTree], 200);
    }

    public function getHadeesBooks()
    {

        try {
            return response()->json(['status' => true, 'data' => HadeesResource::collection(Hadees::all())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
//        $books = [
//            [
//                "id"        => 1,
//                "title"     => "Nahj ul Balagha",
//                "source"    => "https://imam.org.uk/wp-content/uploads/2016/10/Nahjul-Balagha.pdf",
//                "icon"      => asset('frontend/hadees/sahih-al-bukhari.png'),
//            ],
//        ];


    }

}
