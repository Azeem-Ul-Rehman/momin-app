<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;

use App\Mail\ContactUsEmail;
use App\Mail\VerificationOTP;
use App\Models\Role;
use App\Models\Setting;

use App\Models\UserRole;
use App\Resolvers\SocialUserResolver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public $successStatus = 200;

    private $socialLoginUserResolver;
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    /**
     *
     * Inject your repository or the interface here
     */
    public function __construct(SocialUserResolver $socialLoginUserResolver)
    {
        $this->socialLoginUserResolver = $socialLoginUserResolver;
    }

    public function otp(Request $request)
    {
        $details = ['email' => $request->email];

        $validator = Validator::make($details, [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $otp_code = null;
            $user = User::where('email', $request->email)->first();

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);


            $data = [
                'email' => $user->email,
                'code' => $otp_code,
            ];
            $this->verificationEmail($user, $otp_code);

            return response()->json(['status' => true, 'message' => 'OTP code has been sent on given email address.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function verificationEmail($user, $otp_code)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your OTP Code: ' . $otp_code,
            'from' => config('app.admin'),
            'subject' => 'Reset Password'

        ];
        Mail::to($user->email)->send(new VerificationOTP($details));
    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {


            $user = User::where([
                'email' => $request->get('email'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->save();

                return response()->json(['status' => true, 'message' => 'OTP Code verified successfully.'], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'Provided OTP code is not correct.'], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function forgotPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = User::where('email', $request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(["status" => false, "message" => 'Your account is not associated with this email address.'], 404);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['status' => true, "message" => 'Your password has been changed successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|min:6'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {


            $user = Auth::user();
//            if (!$user->activated) {
//                auth()->logout();
//                return response()->json(['message' => 'You need to confirm your account. We have sent you an activation email.'], 401);
//            } else {

            if ($user->status == 'suspended') {
                auth()->logout();
                return response()->json(['message' => 'Your account is suspended.'], 401);
            }

            $success['token'] = $user->createToken('AppName')->accessToken;
            $success['user'] = new UserResource($user);
            $success['settings'] = Setting::get();
            return response()->json([
                'status' => true,
                'token' => $success['token'],
                'settings' => $success['settings'],
                'data' => $success['user']
            ], $this->successStatus);
//            }
        } else {
            return response()->json(['message' => 'Invalid Password'], 401);
        }
    }

    public function logout()
    {
        try {
            $user = Auth::user();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }

            return response()->json(['status' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

        return response()->json(null, 204);
    }

    public function getUser()
    {
        $user = Auth::user();
        return response()->json(['status' => true, 'data' => $user], $this->successStatus);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'phone_number' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'language' => 'required',

        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'phone_number.required' => 'Phone Number is required.',
            'email.required' => 'Email is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }


        $role = Role::find(2);
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $role->id,
            'phone_number' => $request->get('phone_number'),
            'user_type' => $role->name,
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'country' => $request->get('country'),
            'state' => $request->get('state'),
            'city' => $request->get('city'),
            'language' => $request->get('language'),

        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);


        $userData = User::where('email', $request->email)->first();
        $token = $user->createToken('AppName')->accessToken;
        $message = 'Thank you for becoming a member of Momin International';
        return response()->json(['status' => true, 'message' => $message, 'data' => new UserResource($userData), 'token' => $token], 201);
    }


    public function socialLoginRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'provider' => 'required',
            'access_token' => 'required'
        ], [
            'provider.required' => 'Provider is required',
            'access_token.required' => 'Access Token is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()], 400);
        }
        $user = $this->socialLoginUserResolver->resolveUserByProviderCredentials($request->provider, $request->access_token);
//        $success['token'] = $user->createToken('AppName')->accessToken;
//        $success['user'] = new UserResource($user);
//        $success['settings'] = Setting::get();
        return response()->json([
            'status' => true,
//            'token' => $success['token'],
//            'settings' => $success['settings'],
            'data' => $user
        ], $this->successStatus);
    }


}
