<?php


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Event;
use App\Models\EventGallery;
use App\Models\EventGoing;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function __construct()
    {

        $this->middleware('role:admin');
    }

    public function index()
    {

        $events = Event::orderBy('id', 'DESC')->withCount('goings')->get();
        return view('backend.events.index', compact('events'));
    }

    public function create()
    {
        return view('backend.events.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'event_date' => 'required',
//            'location' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'status' => 'required',
            'image' => 'required'

        ], [
            'title.required' => 'Title is required.',
//            'location.required' => 'Location is required.',
            'description.required' => 'Description is required.',
            'status.required' => 'Status is required.',
            'event_date.required' => 'Event Date is required.',
            'start_time.required' => 'Start Time is required.',
            'end_time.required' => 'End Time is required.',
            'address.required' => 'Address is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/events');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        $event = Event::create([
            'title' => $request->title,
            'description' => $request->description,
//            'location' => $request->location,
            'address' => $request->address,
            'event_date' => date('Y-m-d', strtotime($request->event_date)),
            'start_time' => date("H:i", strtotime($request->start_time)),
            'end_time' => date("H:i", strtotime($request->end_time)),
            'status' => $request->status,
            'user_id' => Auth::id(),
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event created successfully.'
            ]);
    }

    public function edit($id)
    {
        $event = Event::find($id);
        return view('backend.events.edit', compact('event'));


    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
//            'location' => 'required',
            'description' => 'required',
            'address' => 'required',
            'event_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'status' => 'required',

        ], [
            'title.required' => 'Title is required.',
//            'location.required' => 'Location is required.',
            'description.required' => 'Description is required.',
            'status.required' => 'Status is required.',
            'event_date.required' => 'Event Date is required.',
            'start_time.required' => 'Start Time is required.',
            'end_time.required' => 'End Time is required.',
            'address.required' => 'Address is required.',
        ]);

        $event = Event::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/events');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $event->image;
        }
        $event->update([
            'title' => $request->title,
//            'location' => $request->location,
            'description' => $request->description,
            'address' => $request->address,
            'event_date' => date('Y-m-d', strtotime($request->event_date)),
            'start_time' => date("H:i", strtotime($request->start_time)),
            'end_time' => date("H:i", strtotime($request->end_time)),
            'status' => $request->status,
            'user_id' => Auth::id(),
            'image' => $profile_image,
        ]);

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->galleries()->delete();
        $event->delete();

        return redirect()->route('admin.events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event has been deleted'
            ]);
    }


    public function updateStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        Event::where('id', $request->get('id'))->update([
            'status' => $request->get('status'),
        ]);

        $response['status'] = 'success';

        return $response;

    }

    public function addEventGallery($event_id)
    {

        return view('backend.events..gallery.create', compact('event_id'));

    }

    public function storeEventGallery(Request $request)
    {

        ini_set('upload_max_filesize', '100M');
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images/gallery'), $imageName);

        $imageUpload = new EventGallery();
        $imageUpload->image = $imageName;
        $imageUpload->event_id = $request->event_id;
        $imageUpload->save();

        if ($image) {
            return response()->json($image, 200);
        } // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    public function viewEventGallery($event_id)
    {

        $event = Event::where('id', $event_id)->with('galleries')->first();
        return view('backend.events.gallery.view', compact('event_id', 'event'));
    }

    public function destroyEventGallery(Request $request)
    {
        $filename = $request->get('filename');
        EventGallery::where('image', $filename)->delete();
        $path = public_path() . '/images/gallery/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }

    public function singleDestroyEventGallery($event_id)
    {

        $event = EventGallery::where('id', $event_id)->first();
        $path = public_path() . '/images/gallery/' . $event->image;
        if (file_exists($path)) {
            unlink($path);
            $event->delete();
        }
        return redirect()->back()
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Event Gallery Image has been deleted'
            ]);
    }


}
