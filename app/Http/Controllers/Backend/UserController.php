<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Realtor;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('backend.users.create', compact( 'roles'));
    }

    public function store(Request $request)
    {

        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'phone_number' => 'required|unique:users,phone_number',
            'home_number' => 'required|unique:users,emergency_number',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',

        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'username.required' => 'Username is required.',
            'role_id.required' => 'Role is required.',
            'phone_number.required' => 'Phone Number is required.',
            'home_number.required' => 'Home Number is required.',
            'status.required' => 'Status is required.',
            'email.required' => 'Email is required.',
        ]);


        if ($request->get('role_id') == 2) {
            $profile_image = 'default.png';
        } else {
            if ($request->has('image')) {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $profile_image = $name;
            } else {

                $profile_image = 'default.png';
            }
        }

        $role = Role::find((int)$request->get('role_id'));


        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'role_id' => $request->get('role_id'),
            'phone_number' => $request->get('phone_number'),
            'emergency_number' => $request->get('home_number'),
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'profile_pic' => $profile_image,
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        Realtor::create([
            'user_id' => $user->id,
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User created successfully.'
            ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('backend.users.edit', compact('user', 'roles'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            'image' => 'image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'phone_number' => 'required',
            'home_number' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255',
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'username.required' => 'Username is required.',
            'phone_number.required' => 'Phone Number is required.',
            'home_number.required' => 'Home Number is required.',
            'status.required' => 'Status is required.',
            'email.required' => 'Email is required.',

        ]);

        $user = User::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $user->profile_pic;
        }


        $role = Role::find((int)$request->get('role_id'));


        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'role_id' => $request->get('role_id'),
            'phone_number' => $request->get('phone_number'),
            'emergency_number' => $request->get('home_number'),
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'profile_pic' => $profile_image,
        ]);
        (new \App\Models\UserRole)->update([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User has been deleted'
            ]);
    }

}
