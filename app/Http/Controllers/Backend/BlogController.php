<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogComment;
use App\Models\User;
use App\Services\BlogComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    protected $blogCommentsService;
    public function __construct(BlogComments $blogCommentsService)
    {
        $this->blogCommentsService = $blogCommentsService;
        $this->middleware('role:admin');
    }

    public function index()
    {
        $blogs = Blog::orderBy('id', 'DESC')->get();
        return view('backend.blogs.index', compact('blogs'));
    }

    public function create()
    {
        return view('backend.blogs.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',
            'status'            => 'required',
            'image'             => 'required'

        ], [
            'title.required'         => 'Title is required.',
            'description.required'   => 'Description is required.',
            'status.required'        => 'Status is required.',
        ]);




        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/blogs');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        $blog = Blog::create([
            'title'         => $request->title,
            'description'   => $request->description,
            'status'        => $request->status,
            'user_id'       => Auth::id(),
            'image'         => $profile_image,
        ]);

        return redirect()->route('admin.blogs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Blog created successfully.'
            ]);
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('backend.blogs.edit', compact('blog'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',
            'status'            => 'required',

        ], [
            'title.required'         => 'Title is required.',
            'description.required'   => 'Description is required.',
            'status.required'        => 'Status is required.',
        ]);

        $blog = Blog::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/blogs');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $blog->image;
        }
        $blog->update([
            'title'         => $request->title,
            'description'   => $request->description,
            'status'        => $request->status,
            'user_id'       => Auth::id(),
            'image'         => $profile_image,
        ]);

        return redirect()->route('admin.blogs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Blog updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->comments()->delete();
        $blog->delete();

        return redirect()->route('admin.blogs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Blog has been deleted'
            ]);
    }

    public function addComment(Request $request)
    {
        $this->validate($request, [
            'comments'             => 'required',

        ], [
            'comments.required'    => 'Comment is required.',
        ]);


        $blog   =  $this->blogCommentsService->comments($request->all(),Auth::id());

        return redirect()->route('admin.blogs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Blog Comment Added Successfully.'
            ]);
    }

    public function viewComments($id)
    {
        $blog = Blog::find($id)->with('comments','comments.user')->first();
        return view('backend.blogs.viewComments', compact('blog'));


    }

    public function updateStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        BlogComment::where('id', $request->get('id'))->update([
            'status' => $request->get('status'),
        ]);

        $response['status'] = 'success';

        return $response;

    }

    public function deleteComment($id)
    {
        $blogComment = BlogComment::findOrFail($id);
        $blogComment->delete();

        return redirect()->back()
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Blog Comment has been deleted'
            ]);
    }

}
