<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Realtor;

class RealtorController extends Controller
{
    public function index()
    {
        $staffs = Realtor::all();
        return view('backend.assign_driver.index', compact('staffs'));
    }
}
