<?php

namespace App\Http\Controllers\Backend;
use App\Models\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('id', 'DESC')->get();
        return view('backend.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'short_summary' => 'required',
            'quoted_by' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ], [
            'short_summary.required' => 'Short Summary is required',
            'quoted_by.required' => 'Quoted By is required',
            'image.required' => 'Please Select valid image ',
        ]);


        $Banners = Banner::all();


        if (count($Banners) > 3) {
            return redirect()->route('admin.banner.index')
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'You can only add 3 Banners'
                ]);
        } else {

            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/banner');
            $image->move($destinationPath, $name);
            $image_name = $name;
            Banner::create([
                'short_summary' => $request->short_summary,
                'quoted_by' => $request->quoted_by,
                'image' => $image_name,
            ]);

            return redirect()->route('admin.banner.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Banner created successfully.'
                ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::whereId($id)->first();
        return view('backend.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'short_summary' => 'required',
            'quoted_by' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:2048',
        ], [
            'short_summary.required' => 'Short Summary is required',
            'quoted_by.required' => 'Quoted By is required',
            'image.required' => 'Please Select valid image ',
        ]);

        $other = Banner::whereId($id)->first();

        if($request->image){
            $image_path = public_path('uploads/banner/').$other->image;

            if(file_exists($image_path)){
                unlink($image_path);
            }
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/banner');
            $image->move($destinationPath, $name);
            $image_name = $name;
        }else{
            $image_name = $other->image;
        }

        $other->fill([
            'short_summary' => $request->short_summary,
            'quoted_by' => $request->quoted_by,
            'image' => $image_name,
        ]);
        $other->save();
        return redirect()->route('admin.banner.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Banner updated successfully.'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Banner = Banner::findOrFail($id);
        $image_path = public_path('uploads/banner/').$Banner->image;

        if(file_exists($image_path)){
            unlink($image_path);
        }

        $Banner->delete();
        return redirect()->route('admin.banner.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Banner has been deleted'
            ]);
    }
}


