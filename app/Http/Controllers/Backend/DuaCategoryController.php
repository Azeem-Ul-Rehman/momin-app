<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\DuaCategory;
use Illuminate\Http\Request;

class DuaCategoryController extends Controller
{
    public function index()
    {
        $categories = DuaCategory::orderBy('id', 'DESC')->get();
        return view('backend.dua.category.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.dua.category.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'             => 'required',

        ], [
            'title.required'         => 'Title is required.',
        ]);

        $category = DuaCategory::create([
            'title'    => $request->title,
        ]);

        return redirect()->route('admin.dua-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua Category created successfully.'
            ]);
    }

    public function edit($id)
    {
        $category = DuaCategory::find($id);
        return view('backend.dua.category.edit', compact('category'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
        ], [
            'title.required'    => 'Title is required.',
        ]);

        $category = DuaCategory::find($id);
        $category->update([
            'title'   => $request->title,
        ]);

        return redirect()->route('admin.dua-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua Category updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $category = DuaCategory::findOrFail($id);
        $category->subcategories()->delete();
        $category->delete();

        return redirect()->route('admin.dua-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua Category has been deleted'
            ]);
    }
}
