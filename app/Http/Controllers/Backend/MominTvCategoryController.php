<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MominTvCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MominTvCategoryController extends Controller
{
    public function index()
    {
        $categories = MominTvCategory::orderBy('id', 'DESC')->with('videos')->get();
        return view('backend.momin-tv.category.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.momin-tv.category.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:momin_tv_categories,name'

        ], [
            'name.required' => 'Name is required.',
        ]);

        $category = MominTvCategory::create([
            'name' => $request->name,
        ]);

        return redirect()->route('admin.momin-tv.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Momin TV Category created successfully.'
            ]);
    }

    public function edit($id)
    {
        $category = MominTvCategory::find($id);
        return view('backend.momin-tv.category.edit', compact('category'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required',
                Rule::unique('momin_tv_categories')->where(function ($query) use ($request,$id) {
                    $query->where('id', '!=', $id);
                })
            ],
        ], [
            'name.required' => 'Name is required.',
        ]);

        $category = MominTvCategory::find($id);
        $category->update([
            'name' => $request->name,
        ]);

        return redirect()->route('admin.momin-tv.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Momin TV Category updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $category = MominTvCategory::findOrFail($id);
        $category->videos()->delete();
        $category->delete();

        return redirect()->route('admin.momin-tv.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Momin TV Category has been deleted'
            ]);
    }
}
