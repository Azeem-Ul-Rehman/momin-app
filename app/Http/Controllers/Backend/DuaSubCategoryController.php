<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\DuaCategory;
use App\Models\DuaSubCategory;
use Illuminate\Http\Request;

class DuaSubCategoryController extends Controller
{
    public function index()
    {
        $subCategories = DuaSubCategory::orderBy('id', 'DESC')->with('category')->get();
        return view('backend.dua.subcategory.index', compact('subCategories'));
    }

    public function create()
    {
        $categories = DuaCategory::orderBy('id', 'DESC')->get();
        return view('backend.dua.subcategory.create',compact('categories'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'             => 'required',
            'category_id'       => 'required',
            'description_en'    => 'required',
//            'audio_en'          => 'required',
            'description_ar'    => 'required',
//            'audio_ar'          => 'required',
            'description_ur'    => 'required',
//            'audio_ur'          => 'required',

        ], [
            'title.required'                => 'Name is required.',
            'category_id.required'          => 'Category is required.',
            'description_en.required'       => 'English Description is required.',
//            'audio_en.required'             => 'English Audio is required.',
            'description_ar.required'       => 'Arabic Description is required.',
//            'audio_ar.required'             => 'Arabic Audio is required.',
            'description_ur.required'       => 'Urdu Description is required.',
//            'audio_ur.required'             => 'Urdu Audio is required.',
        ]);

        $subCategory = DuaSubCategory::create([
            'title'             => $request->title,
            'category_id'       => $request->category_id,
            'description_en'    => $request->description_en,
            'audio_en'          => $request->audio_en,
            'description_ar'    => $request->description_ar,
            'audio_ar'          => $request->audio_ar,
            'description_ur'    => $request->description_ur,
            'audio_ur'          => $request->audio_ur,
        ]);

        return redirect()->route('admin.dua-sub-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua SubCategory created successfully.'
            ]);
    }

    public function edit($id)
    {
        $subcategory = DuaSubCategory::find($id);
        $categories = DuaCategory::orderBy('id', 'DESC')->get();
        return view('backend.dua.subcategory.edit', compact('subcategory','categories'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'category_id'       => 'required',
            'description_en'    => 'required',
//            'audio_en'          => 'required',
            'description_ar'    => 'required',
//            'audio_ar'          => 'required',
            'description_ur'    => 'required',
//            'audio_ur'          => 'required',

        ], [
            'title.required'                => 'Name is required.',
            'category_id.required'          => 'Category is required.',
            'description_en.required'       => 'English Description is required.',
//            'audio_en.required'             => 'English Audio is required.',
            'description_ar.required'       => 'Arabic Description is required.',
//            'audio_ar.required'             => 'Arabic Audio is required.',
            'description_ur.required'       => 'Urdu Description is required.',
//            'audio_ur.required'             => 'Urdu Audio is required.',
        ]);
        $subcategory = DuaSubCategory::find($id);
        $subcategory->update([
            'title'             => $request->title,
            'category_id'       => $request->category_id,
            'description_en'    => $request->description_en,
//            'audio_en'          => $request->audio_en,
            'description_ar'    => $request->description_ar,
//            'audio_ar'          => $request->audio_ar,
            'description_ur'    => $request->description_ur,
//            'audio_ur'          => $request->audio_ur,
        ]);

        return redirect()->route('admin.dua-sub-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua SubCategory updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $subcategory = DuaSubCategory::findOrFail($id);
        $subcategory->delete();

        return redirect()->route('admin.dua-sub-categories.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Dua SubCategory has been deleted'
            ]);
    }
}
