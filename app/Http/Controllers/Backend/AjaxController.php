<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\City;
use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class AjaxController extends Controller
{
    use GeneralHelperTrait;

    public function example(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
        ], [

        ]);

        if (!$validator->fails()) {


            $response['status'] = 'success';
            $response['data'] = [

            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function stateCities(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'state_id' => 'required|exists:states,id'
        ], [
            'state_id.required' => "State is Required.",
            'state_id.exists' => "Invalid State Selected."
        ]);

        if (!$validator->fails()) {
            $cities = City::where('state_id', $request->state_id)->orderBy('name', 'asc')->get();

            $response['status'] = 'success';
            $response['data'] = [
                'cities' => $cities,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }



}
