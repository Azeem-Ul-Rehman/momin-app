<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\MetaTag;
use Illuminate\Http\Request;

class MetaTagController extends Controller
{
    public function index()
    {
        $meta_tags = MetaTag::all();

        return view('backend.meta-tags.index', compact('meta_tags'));
    }


    public function create()
    {
        return view('backend.meta-tags.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'route' => 'required',
            'description' => 'required',
            'keywords' => 'required'
        ]);
        MetaTag::create([
            'title' => $request->title,
            'slug' => $request->title,
            'route' => $request->route,
            'description' => $request->description,
            'keywords' => $request->keywords,
        ]);
        return redirect()->route('admin.meta-tags.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Meta Tag created successfully.'
            ]);
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $meta_tags = MetaTag::find($id);
        return view('backend.meta-tags.edit', compact('meta_tags'));
    }

    public function update(Request $request, $id)
    {
        $meta_tags = MetaTag::find($id);
        $this->validate($request, [
            'title' => 'required',
            'route' => 'required',
            'description' => 'required',
            'keywords' => 'required'
        ]);
        $meta_tags->update([
            'title' => $request->title,
            'slug' => $request->title,
            'route' => $request->route,
            'description' => $request->description,
            'keywords' => $request->keywords,
        ]);
        return redirect()->route('admin.meta-tags.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Meta Tag updated successfully.'
            ]);
    }


    public function destroy($id)
    {
    }
}
