<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\MominTvCategory;
use App\Models\MominTvVideo;
use Illuminate\Http\Request;

class MominTvVideoController extends Controller
{
    public function index()
    {
        $subCategories = MominTvVideo::orderBy('id', 'DESC')->with('category')->get();
        return view('backend.momin-tv.videos.index', compact('subCategories'));
    }

    public function create()
    {
        $categories = MominTvCategory::orderBy('id', 'DESC')->get();
        return view('backend.momin-tv.videos.create',compact('categories'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'                      => 'required',
            'category_id'                => 'required',
            'short_description'          => 'required',
            'link'                       => 'required',
            'image'                      => 'required'

        ], [
            'title.required'                => 'Name is required.',
            'category_id.required'          => 'Category is required.',
            'description_en.required'       => 'English Description is required.',
            'short_description.required'    => 'English Audio is required.',
            'link.required'                 => 'Arabic Description is required.',
        ]);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/video_thumbnail');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }
        $subCategory = MominTvVideo::create([
            'title'                     => $request->title,
            'momin_tv_category_id'      => $request->category_id,
            'short_description'         => $request->short_description,
            'link'                      => $request->link,
            'image'                     => $profile_image,
        ]);

        return redirect()->route('admin.videos.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Video created successfully.'
            ]);
    }

    public function edit($id)
    {
        $subcategory = MominTvVideo::find($id);
        $categories = MominTvCategory::orderBy('id', 'DESC')->get();
        return view('backend.momin-tv.videos.edit', compact('subcategory','categories'));


    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title'                      => 'required',
            'category_id'                => 'required',
            'short_description'          => 'required',
            'link'                       => 'required',

        ], [
            'title.required'                => 'Name is required.',
            'category_id.required'          => 'Category is required.',
            'description_en.required'       => 'English Description is required.',
            'short_description.required'    => 'English Audio is required.',
            'link.required'                 => 'Arabic Description is required.',
        ]);
        $subcategory = MominTvVideo::find($id);

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/video_thumbnail');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $subcategory->image;
        }
        $subcategory->update([
            'title'                     => $request->title,
            'momin_tv_category_id'      => $request->category_id,
            'short_description'         => $request->short_description,
            'link'                      => $request->link,
            'image'                     => $profile_image,
        ]);

        return redirect()->route('admin.videos.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Video updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $subcategory = MominTvVideo::findOrFail($id);
        $subcategory->delete();

        return redirect()->route('admin.videos.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Video has been deleted'
            ]);
    }
}
