<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use App\Models\CalenderEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalenderEventController extends Controller
{
    public function __construct()
    {

        $this->middleware('role:admin');
    }

    public function index()
    {
        $events = CalenderEvent::orderBy('id', 'DESC')->get();
        return view('backend.calendar-events.index', compact('events'));
    }

    public function create()
    {
        return view('backend.calendar-events.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'english_date' => 'required',
            'islamic_date' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'english_date.required' => 'English Date is required.',
            'islamic_date.required' => 'Islamic Date is required.',
        ]);
        $calenderEvents = CalenderEvent::create([
            'title' => $request->title,
            'english_date' => date('Y-m-d', strtotime($request->english_date)),
            'islamic_date' => $request->islamic_date,
        ]);

        return redirect()->route('admin.calendar-events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Calender Event created successfully.'
            ]);
    }

    public function edit($id)
    {
        $event = CalenderEvent::find($id);
        return view('backend.calendar-events.edit', compact('event'));


    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'english_date' => 'required',
            'islamic_date' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'english_date.required' => 'English Date is required.',
            'islamic_date.required' => 'Islamic Date is required.',
        ]);

        $calenderEvents = CalenderEvent::find($id);

        $calenderEvents->update([
            'title' => $request->title,
            'english_date' => date('Y-m-d', strtotime($request->english_date)),
            'islamic_date' => $request->islamic_date,
        ]);

        return redirect()->route('admin.calendar-events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Calender Event updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $calenderEvents = CalenderEvent::findOrFail($id);
        $calenderEvents->delete();

        return redirect()->route('admin.calendar-events.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Calender Event has been deleted'
            ]);
    }


}
