<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        ini_set('max_execution_time', 300);


        if (!is_null($request->state_id)) {

            $cities = DB::table('cities')->where('state_id', (int)$request->state_id)->orderBy('name', 'ASC')->get();

        } else {
            $cities = [];
        }
        $states = State::orderBy('name', 'ASC')->get();

//        $cities =DB::table('cities')->select('id','name','state_id')->orderBy('name', 'ASC')->limit(100000)->get();

        return view('backend.cities.index', compact('cities', 'states'));
    }

    public function create()
    {
        $states = State::orderBy('name', 'ASC')->get();
        return view('backend.cities.create', compact('states'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'state_id' => 'required',
        ]);
        City::create($request->all());
        return redirect()->route('admin.cities.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'City created successfully.'
            ]);

    }

    public function edit($id)
    {
        $city = City::find($id);
        $states = State::orderBy('name', 'ASC')->get();
        return view('backend.cities.edit', compact('city', 'states'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'state_id' => 'required',
        ]);
        City::find($id)->update($request->all());
        return redirect()->route('admin.cities.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'City updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();

        return redirect()->route('admin.cities.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'City has been deleted'
            ]);
    }
}
