<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\AaleMuhammad;
use Illuminate\Http\Request;

class AaleMuhammadController extends Controller
{
    public function index()
    {
        $getData = AaleMuhammad::all();
        return view('backend.aal-e-muhammad.index', compact('getData'));
    }

    public function create()
    {
        return view('backend.aal-e-muhammad.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',

        ], [
            'title.required'         => 'Title is required.',
            'description.required'   => 'Description is required.',
        ]);


        AaleMuhammad::create([
            'title'         => $request->title,
            'description'   => $request->description,
        ]);

        return redirect()->route('admin.aal-e-muhammad.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Record created successfully.'
            ]);
    }

    public function edit($id)
    {
        $data = AaleMuhammad::find($id);
        return view('backend.aal-e-muhammad.edit', compact('data'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'             => 'required',
            'description'       => 'required',

        ], [
            'title.required'         => 'Title is required.',
            'description.required'   => 'Description is required.',

        ]);

        $data = AaleMuhammad::find($id);
        $data->update([
            'title'         => $request->title,
            'description'   => $request->description,
        ]);

        return redirect()->route('admin.aal-e-muhammad.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Record updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $data = AaleMuhammad::findOrFail($id);
        $data->delete();

        return redirect()->route('admin.aal-e-muhammad.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Record has been deleted'
            ]);
    }
}
