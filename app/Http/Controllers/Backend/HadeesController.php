<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Hadees;
use Illuminate\Http\Request;


class HadeesController extends Controller
{
    public function index()
    {
        $hadees = Hadees::orderBy('id', 'DESC')->get();
        return view('backend.hadees.index', compact('hadees'));
    }

    public function create()
    {
        return view('backend.hadees.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title'            => 'required',
            'source'           => 'required',
            'image'             => 'required'

        ], [
            'title.required'       => 'Title is required.',
            'source.required'      => 'Link is required.',
            'image.required'        => 'Icon is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/hadees');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }

        $hadees = Hadees::create([
            'title'         => $request->title,
            'source'        => $request->source,
            'icon'          => $profile_image,
        ]);

        return redirect()->route('admin.hadees.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Hadees Book created successfully.'
            ]);
    }

    public function edit($id)
    {
        $hadees = Hadees::find($id);
        return view('backend.hadees.edit', compact('hadees'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'            => 'required',
            'source'           => 'required',
        ], [
            'title.required'       => 'Title is required.',
            'source.required'      => 'Link is required.',
        ]);

        $hadees = Hadees::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/hadees');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $hadees->icon;
        }
        $hadees->update([
            'title'         => $request->title,
            'source'        => $request->source,
            'icon'          => $profile_image,
        ]);

        return redirect()->route('admin.hadees.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Hadees Book updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $hadees = Hadees::findOrFail($id);
        $hadees->delete();

        return redirect()->route('admin.hadees.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Hadees Book has been deleted'
            ]);
    }
}
